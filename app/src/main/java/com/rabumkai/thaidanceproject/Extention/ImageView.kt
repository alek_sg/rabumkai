package adapter.com.extention

import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.util.Log
import android.view.animation.*
import android.widget.ImageView
import com.bumptech.glide.Glide


fun ImageView.setGlowImage( src: Bitmap ) {
    val bmpGlow = highlightImage(src)
    setImageBitmap(bmpGlow)
}

fun ImageView.setGlowImage( drawable: Drawable) {
    val bmpGlow = drawableToBitmap(drawable)
    val bmp = highlightImage(bmpGlow!!)
    setImageBitmap(bmp)
}

fun highlightImage(src: Bitmap): Bitmap? { // create new bitmap, which will be painted and becomes result image
    val bmOut = Bitmap.createBitmap(src.width + 100, src.height + 100, Bitmap.Config.ARGB_8888)

    val offsetXY = IntArray(2)
    var fX = 0f
    var fY = 0f

    // setup canvas for painting
    val canvas = Canvas(bmOut)

    // setup default color
    canvas.drawColor(0, PorterDuff.Mode.DST_OUT)

    // create a blur paint for capturing alpha
    val ptBlur = Paint()
    ptBlur.maskFilter = BlurMaskFilter(5f, BlurMaskFilter.Blur.OUTER)


    // capture alpha into a bitmap
    val bmAlpha = src.extractAlpha(ptBlur, offsetXY)
    fX = offsetXY[0].toFloat()+50
    fY = offsetXY[1].toFloat()+50

    // create a color paint
    val ptAlphaColor = Paint()
    ptAlphaColor.color = -0x1

    // paint color for captured alpha region (bitmap)
    canvas.drawBitmap(bmAlpha, fX, fY, ptAlphaColor)

    // free memory
    bmAlpha.recycle()

    // paint the image source
    canvas.drawBitmap(src, 50f, 50f, null)

    // return out final image
    return bmOut
}

fun drawableToBitmap(drawable: Drawable): Bitmap? {
    if (drawable is BitmapDrawable) {
        return drawable.bitmap
    }
    var width = drawable.intrinsicWidth
    width = if (width > 0) width else 1
    var height = drawable.intrinsicHeight
    height = if (height > 0) height else 1
    val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    drawable.setBounds(0, 0, canvas.width, canvas.height)
    drawable.draw(canvas)
    return bitmap
}

fun ImageView.loadImage(resourceId:Int)
{
    if( resourceId <= 0 ) return

//    try{
//        Picasso.get()
//            .load(resourceId)
//            .into(this)
//    }catch (ex:Exception){
//        Log.e("ImageView", "ex = $ex")
//    }
}

fun ImageView.loadImage(url:String)
{
    if( TextUtils.isEmpty(url) ) {
        return
    }

    Log.e("ImageView-->loadImage", "url = $url")

//    Picasso.get().load(url).networkPolicy(NetworkPolicy.OFFLINE).into(this)
//    Picasso.get().load(url).into(this)

//    Picasso.Builder().build() .load(url).into(this)

}

fun ImageView.loadImage(url:String , width:Int , height:Int)
{
    if( TextUtils.isEmpty(url) ) {
        return
    }

//    Picasso.get()
//        .load(url)
//        .resize(width,height)
//        .into(this);
}

fun ImageView.setColorDisable()
{
    val matrix = ColorMatrix();
    matrix.setSaturation(0.toFloat());
    colorFilter = ColorMatrixColorFilter(matrix);
    isEnabled = false
}

fun ImageView.setColorEnable()
{
    colorFilter = null
    isEnabled = true
}

private fun aniFadeIn():Animation{
    val fadeIn = AlphaAnimation(0f, 1f)
    fadeIn.duration = 2000
    val animation = AnimationSet(false) //change to false
    animation.addAnimation(fadeIn)
    return fadeIn
}

private var fadeIn:Animation? = null
private var fadeOut:Animation? = null

private fun animationFadeIn(iv:ImageView){

    fadeIn = AlphaAnimation(0.1f, 1.0f)
    fadeIn!!.duration = 1000
    fadeIn!!.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(p0: Animation?) {

        }

        override fun onAnimationEnd(p0: Animation?) {
            iv.startAnimation(fadeOut)
        }

        override fun onAnimationStart(p0: Animation?) {
        }

    })
}

fun ImageView.Triangle(bitmap:Bitmap, radius:Int):Bitmap{
    val finalBitmap: Bitmap
    finalBitmap = if (!(bitmap.width <= radius && bitmap.height <= radius)) {
        Bitmap.createScaledBitmap(
            bitmap, radius, radius,
            false
        )
    } else bitmap

    val output = Bitmap.createBitmap(
        finalBitmap.width,
        finalBitmap.height, Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(output)

    val paint = Paint()
    val rect = Rect(
        0, 0, finalBitmap.width,
        finalBitmap.height
    )

    val point1_draw = Point(radius/2, 0)
    val point2_draw = Point(0, radius)
    val point3_draw = Point(radius, radius)

    val path = Path()
    path.moveTo(point1_draw.x.toFloat(), point1_draw.y.toFloat())
    path.lineTo(point2_draw.x.toFloat(), point2_draw.y.toFloat())
    path.lineTo(point3_draw.x.toFloat(), point3_draw.y.toFloat())
    path.lineTo(point1_draw.x.toFloat(), point1_draw.y.toFloat())
    path.close()
    canvas.drawARGB(0, 0, 0, 0)
    paint.color = Color.parseColor("#EE2524")
    canvas.drawPath(path, paint)
    paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
    canvas.drawBitmap(finalBitmap, rect, rect, paint)

    return output
}