package com.rabumkai.thaidanceproject.model


import com.google.gson.annotations.SerializedName

data class GameMatchingModel(
    @SerializedName("game_matching")
    val gameMatching: GameMatching
)