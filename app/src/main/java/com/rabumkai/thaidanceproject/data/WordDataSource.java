package com.rabumkai.thaidanceproject.data;

import com.rabumkai.thaidanceproject.model.Word;

import java.util.List;

/**
 * Created by abdularis on 18/07/17.
 */

public interface WordDataSource {

    List<Word> getWords();

}
