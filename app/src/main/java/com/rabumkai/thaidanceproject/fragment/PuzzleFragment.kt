package com.rabumkai.thaidanceproject.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.rabumkai.thaidanceproject.FillWordActivity
import com.rabumkai.thaidanceproject.GameJigsawActivity
import com.rabumkai.thaidanceproject.GameMatchingActivity
import com.rabumkai.thaidanceproject.R
import com.rabumkai.thaidanceproject.features.SplashScreenActivity
import com.rabumkai.thaidanceproject.features.gameplay.GamePlayActivity
import com.rabumkai.thaidanceproject.features.mainmenu.MainMenuActivity
import com.rabumkai.thaidanceproject.model.QuizModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_puzzle.*
import java.util.*

/**
 * A placeholder fragment containing a simple view.
 */

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class PuzzleFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    private var lstQuestionModel = ArrayList<QuizModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        return inflater.inflate(R.layout.fragment_puzzle, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PuzzleFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindEvent()
    }

    private fun bindEvent(){
        llGameMatching.setOnClickListener {
            val intent = Intent( activity , GameMatchingActivity::class.java )
            startActivity( intent )
        }
        llCrossword.setOnClickListener {
            val intent = Intent(activity, GamePlayActivity::class.java)
            intent.putExtra(GamePlayActivity.EXTRA_ROW_COUNT, 8)
            intent.putExtra(GamePlayActivity.EXTRA_COL_COUNT, 8)
            startActivity(intent)
        }
        llGameJigsaw.setOnClickListener {

            val intent = Intent( activity , GameJigsawActivity::class.java )
            startActivity(intent)
        }
//        llCorrectOrWrong.setOnClickListener {
//            val questionModel = Gson().toJson(lstQuestionModel[2])
//            val intent = Intent( activity , CorrectOrWrongActivity::class.java )
//            intent.putExtra("data" , questionModel)
//            startActivity(intent)
//        }
    }
}