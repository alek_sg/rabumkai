package com.rabumkai.thaidanceproject

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.rabumkai.thaidanceproject.fragment.AboutFragment
import com.rabumkai.thaidanceproject.fragment.ExerciseFragment
import com.rabumkai.thaidanceproject.fragment.PuzzleFragment
import com.rabumkai.thaidanceproject.model.ProfileModel
import com.google.gson.Gson
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_tab.*

class TabActivity : BaseActivity() {

    lateinit var profileModel:ProfileModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab)

        if( !Hawk.isBuilt() ){
            Hawk.init(this).build()
        }

        intent.extras?.apply {
            if( containsKey("profile") ){
                val data = Gson().fromJson(getString("profile") , ProfileModel::class.java)
                profileModel = data
            }
        }

        setDefaultMenu()
        setMenuActive(ivLearning,tvMenuLearning)

        val bundle = intent.extras!!
        if( bundle == null ){
            initialFragment("learning")
        }else{
            if( bundle.containsKey("event") ){
                initialFragment("learning")

                if( bundle.getString("event") == "go_to_history_activity" ){
                    gotoHistoryActivity()
                }else if(bundle.getString("event") == "go_to_acting_activity"){
                    gotoActingActivity()
                }
            }else{
                initialFragment("learning")
            }
        }

        bindEvent()
    }

    private fun gotoPostureActivity(){
        val dataPosture = getPosture()
        val data = Gson().toJson(dataPosture)

        val intent = Intent( this , ParentPostureActivity::class.java )
        intent.putExtra("data" , data )
        intent.putExtra("title" , "นาฏยศัพท์และภาษาท่า" )
        startActivity(intent)
    }

    private fun gotoHistoryActivity(){
        val intent = Intent( this , HistoryActivity::class.java )
        val data = getHistory()
        intent.putExtra("data" , data )
        intent.putExtra("title" , "ประวัติความเป็นมา" )
        startActivity(intent)
    }

    private fun gotoActingActivity(){
        val intent = Intent( this , ActingActivity::class.java )
        intent.putExtra("resource" , R.raw.acting )
        intent.putExtra("title" , "วีดีโอการแสดง" )
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()

        Hawk.destroy()
    }

    private fun setDefaultMenu(){
        ivLearning.setColorFilter(ContextCompat.getColor(this, R.color.color_black_a_12), android.graphics.PorterDuff.Mode.MULTIPLY)
        ivExcercise.setColorFilter(ContextCompat.getColor(this, R.color.color_black_a_12), android.graphics.PorterDuff.Mode.MULTIPLY)
        ivPuzzle.setColorFilter(ContextCompat.getColor(this, R.color.color_black_a_12), android.graphics.PorterDuff.Mode.MULTIPLY)
        ivLogout.setColorFilter(ContextCompat.getColor(this, R.color.color_black_a_12), android.graphics.PorterDuff.Mode.MULTIPLY)

        tvMenuLearning.setTextColor(ContextCompat.getColor(this , R.color.color_black_a_12))
        tvMenuExcercie.setTextColor(ContextCompat.getColor(this , R.color.color_black_a_12))
        tvMenuPuzzle.setTextColor(ContextCompat.getColor(this , R.color.color_black_a_12))
        tvMenuLogout.setTextColor(ContextCompat.getColor(this , R.color.color_black_a_12))
    }

    private fun setMenuActive(iv:ImageView , tvMenu:TextView){
        iv.setColorFilter(ContextCompat.getColor(this, R.color.green), android.graphics.PorterDuff.Mode.MULTIPLY)
        tvMenu.setTextColor(ContextCompat.getColor(this , R.color.green))
    }

    private fun bindEvent(){
        llLearning.setOnClickListener {
            setDefaultMenu()
            setMenuActive(ivLearning , tvMenuLearning)
            initialFragment("learning")
        }
        llExcercise.setOnClickListener {
            setDefaultMenu()
            setMenuActive(ivExcercise , tvMenuExcercie)
            initialFragment( "exercise" )
        }
        llPuzzle.setOnClickListener {
            setDefaultMenu()
            setMenuActive(ivPuzzle , tvMenuPuzzle)
            initialFragment("puzzle")
        }

        llLogout.setOnClickListener {
            setDefaultMenu()
            setMenuActive(ivLogout , tvMenuLogout)
            initialFragment( "about" )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if( requestCode == 9999 ){
            val lstQuizeData = getQuiz()
            val dataQuestion = Gson().toJson(lstQuizeData[0])

            val intent = Intent( this , QuestionActivity::class.java )
            intent.putExtra("data" , dataQuestion)
            intent.putExtra("before_study" , "true")
            intent.putExtra("event" , "after_play_vdo_main_menu_end")
            startActivity(intent)
        }
    }

    private fun initialFragment(pageName: String){
        val fmManager = this@TabActivity.supportFragmentManager
        var newFragment: Fragment? = null
        var transaction: FragmentTransaction? = null
        var tag = ""

        when (pageName) {
            "learning" -> {
                newFragment = MainMenuFragment.newInstance("", "")
                tag = "learning"
            }
            "exercise" -> {
                val data = getQuiz()
                val dataQuestion = Gson().toJson(data)
                newFragment = ExerciseFragment.newInstance(dataQuestion, "")
                tag = "exercise"
            }
            "puzzle" -> {
                newFragment = PuzzleFragment.newInstance("", "")
                tag = "puzzle"
            }

            "about"->{
//                Hawk.deleteAll()
//
//                val intent = Intent( this , LoginActivity::class.java )
//                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
//                startActivity(intent)
//                finish()

                newFragment = AboutFragment.newInstance("", "")
                tag = "about"
            }
        }
        transaction = fmManager.beginTransaction()
        transaction.replace(R.id.flContainerTab, newFragment!!, tag)
        transaction.addToBackStack(tag)
        transaction.commit()
    }
}