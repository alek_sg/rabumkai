package com.rabumkai.thaidanceproject.fragment

import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import androidx.fragment.app.Fragment
import com.rabumkai.thaidanceproject.FullScreenVideoActivity
import com.rabumkai.thaidanceproject.R
import kotlinx.android.synthetic.main.fragment_tab2_song.*

/**
 * A placeholder fragment containing a simple view.
 */

private const val ARG_SEEK_TO = "seek_to"

class Tab2SongFragment : Fragment() {

    private var seekTo = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        arguments?.let {
            seekTo = it.getInt(ARG_SEEK_TO)
        }

        return inflater.inflate(R.layout.fragment_tab2_song, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(seekTo: Int) =
            Tab2SongFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SEEK_TO, seekTo)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindVDO()

        ivFullScreen.setOnClickListener {
            val intent = Intent(activity, FullScreenVideoActivity::class.java)
            intent.putExtra("seek", vdoViewTab2!!.currentPosition)
            intent.putExtra("resource", R.raw.karaoke_compress)
            intent.putExtra("requestCode", 3001)

            startActivityForResult(intent, 3001)
        }

        ivPlayVdoTab2.visibility = View.VISIBLE
        ivPlayVdoTab2.setOnClickListener {
            vdoViewTab2!!.seekTo(seekTo)
            vdoViewTab2!!.start()
            ivPlayVdoTab2.visibility = View.GONE
        }

        Log.e("Tab2SongFragment", "onViewCreated")
    }

    private fun bindVDO(){
        val mediaController = MediaController(activity)
        mediaController.setAnchorView(vdoViewTab2)
        vdoViewTab2!!.setMediaController(mediaController)
        playVdo(R.raw.karaoke_compress, seekTo)
    }

    private fun playVdo(idResource: Int, seekTo: Int){
        try{
            val path = "android.resource://com.rabumkai.thaidanceproject/$idResource"
            vdoViewTab2!!.setVideoURI(Uri.parse(path))
            vdoViewTab2!!.setOnPreparedListener(MediaPlayer.OnPreparedListener { mp ->
                mp.isLooping = false
                mp.setScreenOnWhilePlaying(false)
                vdoViewTab2!!.seekTo(seekTo)

            })
        }catch (e: Exception){
            Log.e("Tab1SongFragment", "playVdo-->$e")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if( data != null ){
            data.extras?.apply {
                if( vdoViewTab2 != null ){
                    playVdo(getInt("resource"), getInt("seek"))
                    vdoViewTab2!!.start()
                }else{
                    Log.e("Tab1SongFragment", "vdoViewTab2 == null")
                }
            }
        }else{
            Log.e("Tab1SongFragment", "data == null")
        }
    }

    override fun onPause() {
        super.onPause()

        if( vdoViewTab2 != null ){
            vdoViewTab2!!.pause()
        }
    }

    override fun onStop() {
        super.onStop()

        if( vdoViewTab2 != null ){
            vdoViewTab2!!.pause()
        }
    }
}