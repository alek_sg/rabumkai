package com.rabumkai.thaidanceproject

import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.MediaController
import androidx.fragment.app.Fragment
import com.adapter.office.Extention.showDialogComplete
import com.adapter.office.Extention.showDialogReadMore
import com.rabumkai.thaidanceproject.model.*
import com.google.firebase.database.*
import com.google.gson.Gson
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_main_menu_new.*
import kotlinx.android.synthetic.main.activity_main_menu_new.tvHistory
import kotlinx.android.synthetic.main.activity_main_menu_new.tvVdoIntroduce
import org.json.JSONObject

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class MainMenuFragment : Fragment() {
    var profileModel:RegisterModel ?= null

    var database: FirebaseDatabase? = null
    var dataSnapshot:DataSnapshot? = null

    val MENU_HISTORY = "1"
    val MENU_POSTURE = "2"
    val MENU_SONG = "3"
    val MENU_ACTING = "4"
    val MENU_COSTUME = "5"
    val MENU_SINGOUT = "6"
    val MENU_MUSIC_INSTRUMENT = "7"

    var jsonObject:JSONObject? = null

    private var param1: String? = null
    private var param2: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        return inflater.inflate(R.layout.activity_main_menu_new, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MainMenuFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if( !Hawk.isBuilt() ){
            Hawk.init(activity).build()
        }

        profileModel = Hawk.get<RegisterModel>("profile")
        bindEvent()

        if( profileModel == null ){
            cvProfile.visibility = View.GONE

            if( !(activity!!.application as BaseApplication).isDoneExerciseBeforeStudy )
                playVdoIntroduce(isGoingHistory = false)
        }
        else{
            if( profileModel!!.is_done_exercise != "true")
                playVdoIntroduce(isGoingHistory = false)

            cvProfile.visibility = View.VISIBLE
            bindProfile()
        }

        tvVdoIntroduce.setOnClickListener {
            playVdoIntroduce(isGoingHistory = true)
        }
    }

    private fun bindEvent(){
        llHistory.setOnClickListener {
            startActivityByMenu(MENU_HISTORY , tvHistory.text.toString())
        }

        llRabum.setOnClickListener {
            startActivityByMenu(MENU_POSTURE , tvRabum.text.toString())
        }

        llSong.setOnClickListener {
            startActivityByMenu(MENU_SONG , tvMusic.text.toString())
        }

        llActing.setOnClickListener {
            startActivityByMenu(MENU_ACTING , tvActing.text.toString())
        }

        llCostume.setOnClickListener {
            startActivityByMenu(MENU_COSTUME , tvCostume.text.toString())
        }

        llMusicEquipment.setOnClickListener {
            startActivityByMenu(MENU_MUSIC_INSTRUMENT , tvMusicEquipment.text.toString())
        }

        btnReadMore.setOnClickListener {
            this.activity.showDialogReadMore().apply {
                show()

                window?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                window?.setBackgroundDrawableResource(android.R.color.transparent);
            }
        }
    }

    private fun playVdoIntroduce(isGoingHistory:Boolean){
        val intent = Intent( activity , FullScreenVideoActivity::class.java )
        intent.putExtra("seek" , 0)
        intent.putExtra("resource" ,R.raw.introduce )
        var parentActivity = activity as BaseActivity

        if( isGoingHistory ){
            intent.putExtra("goto" ,"goto_history" )
            startActivity(intent)
        }
        else{
            intent.putExtra("goto" ,"goto_question" )
            parentActivity.startActivityForResult(intent , 9999 )
        }
    }

    private fun startActivityByMenu(menuId:String, menuTitle:String ){
        val baseActivity = activity as BaseActivity

        when ( menuId ){
            MENU_HISTORY->{
                val intent = Intent( activity , HistoryActivity::class.java )
                val data = baseActivity.getHistory()
                intent.putExtra("data" , data )
                intent.putExtra("title" , menuTitle )
                startActivity(intent)
            }
            MENU_POSTURE->{
                val dataPosture = baseActivity.getPosture()
                val data = Gson().toJson(dataPosture)

//                val intent = Intent( this@MainMenuActivity , MenuPostureActivity::class.java )
                val intent = Intent( activity , ParentPostureActivity::class.java )
                intent.putExtra("data" , data )
                intent.putExtra("title" , menuTitle )
                startActivity(intent)
            }
            MENU_SONG->{
                val dataSong = baseActivity.getSong()
                val dataVocab = baseActivity.getDataVocab()
                val data = Gson().toJson(dataSong)
                val vocabData = Gson().toJson(dataVocab)
                val title = menuTitle

                val intent = Intent( activity , ParentSongActivity::class.java )
                intent.putExtra("data" , data )
                intent.putExtra("vocab" , vocabData )
                intent.putExtra("title" , title )
                startActivity(intent)
            }
            MENU_ACTING->{
                val intent = Intent( activity , ActingActivity::class.java )
                val title = menuTitle
                intent.putExtra("resource" , R.raw.acting )
                intent.putExtra("title" , title )
                startActivity(intent)
            }
            MENU_COSTUME->{
                val intent = Intent( activity , CostumeActivity::class.java )
                val data = baseActivity.getCostume()
                val title = menuTitle
                intent.putExtra("data" , data )
                intent.putExtra("title" , title )
                startActivity(intent)
            }
            MENU_MUSIC_INSTRUMENT->{
                val intent = Intent( activity , MusicInstrumentActivity::class.java )
                val lstMusicalInstrumentModel = baseActivity.getMusicalInstrumentModel()
                val title = menuTitle
                val data = Gson().toJson(lstMusicalInstrumentModel)

                intent.putExtra("data" , data )
                intent.putExtra("title" , title )
                startActivity(intent)
            }
            MENU_SINGOUT->{
                Hawk.deleteAll()

                val intent = Intent( activity , LoginActivity::class.java )
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                activity?.finish()
            }
        }
    }

    private fun bindProfile(){
        profileModel?.apply {
            if( name.isEmpty() ){
                tvName.visibility = View.GONE
                tvClassroom.visibility = View.GONE
                cvProfile.visibility = View.GONE
            }else{
                tvName.text = "$name $lastname"
                tvClassroom.text = classroom

                tvName.visibility = View.VISIBLE
                tvClassroom.visibility = View.VISIBLE

                if( name.isEmpty() and lastname.isEmpty() )
                    cvProfile.visibility = View.VISIBLE
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        Hawk.destroy()
    }
}