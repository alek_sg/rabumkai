package com.rabumkai.thaidanceproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.rabumkai.thaidanceproject.model.QuestionModel
import com.rabumkai.thaidanceproject.model.QuizModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_submenu_question.*
import java.util.ArrayList

class SubmenuQuestionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_submenu_question)

        val bundle = intent.extras
        val data = bundle?.getString("data")

        val userListType = object : TypeToken<ArrayList<QuizModel?>?>() {}.type
        val lstQuestionModel:ArrayList<QuizModel> = Gson().fromJson(data, userListType)

        bindView(lstQuestionModel)

    }

    private fun bindView(lstQuestionModel:ArrayList<QuizModel>  ){
        llRoot.removeAllViews()

        for( i in 0..lstQuestionModel.size-1 ){
            val view = layoutInflater.inflate(R.layout.layout_sub_menu,null)
            val tvSubMenu = view.findViewById<TextView>(R.id.tvSubMenu)
            tvSubMenu.text = lstQuestionModel[i].sub_menu
            view.tag = lstQuestionModel[i].sub_menu
            view.setOnClickListener {
                var intent = Intent()
                when (lstQuestionModel[i].sub_menu) {
                    "แบบทดสอบหลังเรียน" -> {
                        val questionModel = Gson().toJson(lstQuestionModel[i])
                        intent = Intent( this@SubmenuQuestionActivity , QuestionActivity::class.java )
                        intent.putExtra("before_study" , "false")
                        intent.putExtra("data" , questionModel)
                    }
                    "แบบทดสอบก่อนเรียน" -> {

                        val questionModel = Gson().toJson(lstQuestionModel[i])
                        intent = Intent( this@SubmenuQuestionActivity , QuestionActivity::class.java )
                        intent.putExtra("before_study" , "true")
                        intent.putExtra("data" , questionModel)
                    }
                    "แบบฝึกหัดแบบเติมคำ" -> {
                        val questionModel = Gson().toJson(lstQuestionModel[i])
                        intent = Intent( this@SubmenuQuestionActivity , FillWordActivity::class.java )
                        intent.putExtra("data" , questionModel)
                        intent.putExtra("title" , "แบบฝึกหัดที่ 1")
                    }
                    "แบบฝึกหัดแบบถูกผิด" -> {
                        val questionModel = Gson().toJson(lstQuestionModel[i])
                        intent = Intent( this@SubmenuQuestionActivity , CorrectOrWrongActivity::class.java )
                        intent.putExtra("data" , questionModel)
                    }
                }

                startActivity(intent)
            }

            llRoot.addView(view)
        }
    }
}