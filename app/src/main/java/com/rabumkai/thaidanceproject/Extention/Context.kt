package com.adapter.office.Extention


import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.rabumkai.thaidanceproject.R
import org.jetbrains.anko.AlertBuilder
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog

fun Context?.toast(text: String) = this?.let { Toast.makeText(it, text, Toast.LENGTH_LONG).show() }
fun Context?.toastShot(text: String) = this?.let { Toast.makeText(it, text, Toast.LENGTH_SHORT).show() }
fun Context?.indeterminateProgress(text: String): ProgressDialog? {
    this?.let {
        val dialog = this.indeterminateProgressDialog(text)
        dialog.window!!.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog.window!!.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT) )
        dialog.setContentView(R.layout.layout_progress)
        dialog.setCancelable(false)
        return dialog
    }
    return null
}

fun Context.alert(title: String, message: String): AlertBuilder<AlertDialog> {
    return this.alert(message, title)
}

fun Context?.showLargeImage():Dialog{
    this?.let{
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.layout_dialog_large_image)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)


        return dialog
    }

    return null!!
}

fun Context?.showVdo():Dialog{
    this?.let{
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.layout_dialog_vdo)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)

        return dialog
    }

    return null!!
}
fun Context?.showPosture():Dialog{
    this?.let{
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.layout_dialog_posture)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)

        return dialog
    }

    return null!!
}

fun Context?.showVocab():Dialog{
    this?.let{
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.layout_dialog_vocab)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)

        return dialog
    }

    return null!!
}

fun Context?.showAnswer():Dialog{
    this?.let{
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.layout_dialog_answer)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)

        return dialog
    }

    return null!!
}
fun Context?.showCorrectQuestion(myAnswer:String,question:String):Dialog{
    this?.let{
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.layout_dialog_question_correct)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.findViewById<TextView>(R.id.tvMyAnswer).text = myAnswer
        dialog.findViewById<TextView>(R.id.tvQuestion).text = question
        dialog.show()

        return dialog
    }

    return null!!
}

fun Context?.showDialogWarning(message:String,textPositive:String):Dialog{
    this?.let{
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.layout_dialog_warning)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.findViewById<TextView>(R.id.tvTitle).text = message
        dialog.findViewById<TextView>(R.id.tvPositive).text = textPositive
        dialog.show()

        return dialog
    }

    return null!!
}

fun Context?.showFailedQuestion( question:String , correctAnswer:String , myAnswer:String , isBeforeStudy:Boolean ):Dialog{
    this?.let{
        val dialog = Dialog(this)

        dialog.setContentView(R.layout.layout_dialog_question_failed)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        val tvMyAnswer = dialog.findViewById<TextView>(R.id.tvMyAnswer)
        val tvAnswer = dialog.findViewById<TextView>(R.id.tvAnswer)
        val tvQuestion = dialog.findViewById<TextView>(R.id.tvQuestion)
        val llContainerAnswer = dialog.findViewById<LinearLayout>(R.id.llContainerAnswer)
        val llContainerQuestion = dialog.findViewById<LinearLayout>(R.id.llContainerQuestion)
        val llContainerMyAnswer = dialog.findViewById<LinearLayout>(R.id.llContainerMyAnswer)

        tvAnswer.text = correctAnswer
        tvQuestion.text = question
        tvMyAnswer.text = myAnswer

        if( isBeforeStudy ) {
            llContainerAnswer.visibility = View.GONE
            llContainerQuestion.visibility = View.VISIBLE
            llContainerMyAnswer.visibility = View.VISIBLE
        }
        else{
            llContainerAnswer.visibility = View.VISIBLE
            llContainerQuestion.visibility = View.VISIBLE
            llContainerMyAnswer.visibility = View.VISIBLE
        }

        dialog.show()

        return dialog
    }

    return null!!
}

fun Context?.showMissMatch():Dialog{
    this?.let{
        val dialog = Dialog(this , R.style.PauseDialog)

        dialog.setContentView(R.layout.layout_dialog_miss_match)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)

        dialog.show()

        return dialog
    }

    return null!!
}
fun Context?.showMatch():Dialog{
    this?.let{
        val dialog = Dialog(this , R.style.PauseDialog)

        dialog.setContentView(R.layout.layout_dialog_match)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)

        dialog.show()

        return dialog
    }

    return null!!
}

fun Context?.showDialogComplete():Dialog{
    this?.let{
        val dialog = Dialog(this , R.style.PauseDialog)

        dialog.setContentView(R.layout.layout_complete)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)

        dialog.show()

        return dialog
    }

    return null!!
}
fun Context?.showDialogReadMore():Dialog{
    this?.let{
        val dialog = Dialog(this , R.style.PauseDialog)

        dialog.setContentView(R.layout.layout_read_more)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        var btnClose = dialog.findViewById<Button>(R.id.btnClose)
        btnClose.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()

        return dialog
    }

    return null!!
}

