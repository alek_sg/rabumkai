package com.rabumkai.thaidanceproject.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.rabumkai.thaidanceproject.R
import com.rabumkai.thaidanceproject.fragment.Tab1SongFragment
import com.rabumkai.thaidanceproject.fragment.Tab2SongFragment

private val TAB_TITLES = arrayOf(
    R.string.tab_song_1,
    R.string.tab_song_2
)

class SectionsPagerSongAdapter(private val context: Context, fm: FragmentManager , private val dataVocab:String, private val data:String) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {

        return if( position == 0 )
            Tab1SongFragment.newInstance(dataVocab,data,10)
        else
            Tab2SongFragment()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return 2
    }
}