package com.rabumkai.thaidanceproject.model

data class ProfileModel(val classroom:String,
                        val name:String,
                        val lastname:String ,
                        val is_done_exercise:String ,
                        val is_done_exercise_fill_word:String) {
}