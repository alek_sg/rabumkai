package com.rabumkai.thaidanceproject.adapter;

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import com.rabumkai.thaidanceproject.R
import com.rabumkai.thaidanceproject.model.VocabModel
import kotlinx.android.synthetic.main.item_posture.view.*
import kotlinx.android.synthetic.main.item_vocab.view.*
import java.lang.Exception


class VocabAdapter(val context: Context, val lstVocabModel: ArrayList<VocabModel>, val listener:ClickListener) : RecyclerView.Adapter<VocabAdapter.ViewHolder>() {

    interface ClickListener{
        fun onClickItem( VocabModel: VocabModel )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(lstVocabModel[position] )
        holder.itemView.setOnClickListener {
            listener.onClickItem(lstVocabModel[position])
        }
    }

    override fun getItemCount() = lstVocabModel.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return  ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_vocab , parent,false))
    }

    class ViewHolder(itemsView: View ): RecyclerView.ViewHolder(itemsView) {

        fun bind(item: VocabModel ) {
            itemView.apply {
                try {
                    tvVocab.text = item.vocabulary
                } catch ( ex:Exception) {
                    Log.e("LitsRedeemAdapter" , "ex = "+ex.toString())
                }
            }
        }
    }
}