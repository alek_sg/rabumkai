package com.rabumkai.thaidanceproject

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.rabumkai.thaidanceproject.adapter.PostureAdapter
import com.rabumkai.thaidanceproject.adapter.VocabAdapter
import com.rabumkai.thaidanceproject.model.VocabModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_vocap.*


class VocabActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vocap)

        getListVocabFromFirebase()
    }

    private fun getListVocabFromFirebase(){
        val database = Firebase.database
        val myRef = database.getReference("dev/vocap")
        myRef.addListenerForSingleValueEvent(
            object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    Log.e("LoginActivity","error->detail = "+error.details+"\nerror->message = "+error.message)
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val lstPosture = ArrayList<VocabModel>()
                    for (ds in snapshot.children) {
                        val vocabModel = ds.getValue(VocabModel::class.java)
                        lstPosture.add(vocabModel!!)
                    }

                    bindView(lstPosture)
                }
            })
    }

    private fun bindView( lstPosture:ArrayList<VocabModel>  ){
        val adapter = VocabAdapter(this, lstPosture , object : VocabAdapter.ClickListener {
            override fun onClickItem(VocabModel: VocabModel) {
                val data = Gson().toJson(VocabModel)
                val i = Intent( this@VocabActivity , DetailVocabActivity::class.java )
                i.putExtra("vocab" , data )
                startActivity(i)
            }
        })

        val dividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            LinearLayoutManager.VERTICAL
        )
        recyclerView.addItemDecoration(dividerItemDecoration)
        recyclerView.layoutManager = LinearLayoutManager( this , LinearLayoutManager.VERTICAL , false )
        recyclerView.adapter = adapter
    }
}