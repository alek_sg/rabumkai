package com.rabumkai.thaidanceproject.model


import com.google.gson.annotations.SerializedName

data class ContentPostureImg(
    @SerializedName("img")
    val img: String,
    @SerializedName("posture_name")
    val postureName: String){

    override fun toString(): String {
        return "img->"+img+"\n"+"postureName->"+postureName
    }
}