package com.rabumkai.thaidanceproject.adapter;

import adapter.com.extention.loadImage
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import com.bumptech.glide.Glide
import com.rabumkai.thaidanceproject.BaseActivity
import com.rabumkai.thaidanceproject.R
import com.rabumkai.thaidanceproject.model.Content
import kotlinx.android.synthetic.main.item_posture.view.tvPosture
import kotlinx.android.synthetic.main.layout_posture.view.*
import java.lang.Exception


class PostureAdapter(val context: Context, val lstPostureActionModel: ArrayList<Content>, val listener:ClickListener) : RecyclerView.Adapter<PostureAdapter.ViewHolder>() {

    interface ClickListener{
        fun onClickItem(content: Content)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(lstPostureActionModel[position] )
        holder.itemView.setOnClickListener {
            listener.onClickItem(lstPostureActionModel[position])
        }
    }

    override fun getItemCount() = lstPostureActionModel.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return  ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_posture , parent,false))
    }

    class ViewHolder(itemsView: View ): RecyclerView.ViewHolder(itemsView) {

        fun bind(item: Content ) {
            itemView.apply {
                try {
                    val baseActivity = context as BaseActivity
                    val resource = baseActivity.getResourceId(context , item.img , "drawable" , baseActivity.packageName)
                    Glide.with(baseActivity).load(resource).into(ivPosture)
                    tvPosture.text = item.posture_name
                } catch ( ex:Exception) {
                    Log.e("LitsRedeemAdapter" , "ex = "+ex.toString())
                }
            }
        }
    }
}