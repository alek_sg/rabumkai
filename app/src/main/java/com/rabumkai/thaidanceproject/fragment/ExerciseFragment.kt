package com.rabumkai.thaidanceproject.fragment

import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import androidx.fragment.app.Fragment
import com.rabumkai.thaidanceproject.*
import com.rabumkai.thaidanceproject.model.QuizModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_exercise.*
import java.util.ArrayList

/**
 * A placeholder fragment containing a simple view.
 */

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ExerciseFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    private var lstQuestionModel = ArrayList<QuizModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        return inflater.inflate(R.layout.fragment_exercise, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ExerciseFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val userListType = object : TypeToken<ArrayList<QuizModel?>?>() {}.type
        lstQuestionModel = Gson().fromJson(param1, userListType)

        bindEvent()
    }

    private fun bindEvent(){
        llExercisePreLearning.setOnClickListener {
            val questionModel = Gson().toJson(lstQuestionModel[0])
            val intent = Intent( activity , QuestionActivity::class.java )
            intent.putExtra("before_study" , "true")
            intent.putExtra("data" , questionModel)
            startActivity(intent)
        }
        llExercisePostLearning.setOnClickListener {
            val questionModel = Gson().toJson(lstQuestionModel[3])
            val intent = Intent( activity , QuestionActivity::class.java )
            intent.putExtra("before_study" , "false")
            intent.putExtra("data" , questionModel)
            startActivity(intent)
        }
        llFillWord.setOnClickListener {
            val questionModel = Gson().toJson(lstQuestionModel[1])
            val intent = Intent( activity , FillWordActivity::class.java )
            intent.putExtra("data" , questionModel)
            intent.putExtra("title" , "แบบผึกหัดเติมคำ")
            startActivity(intent)
        }
        llCorrectOrWrong.setOnClickListener {
            val questionModel = Gson().toJson(lstQuestionModel[2])
            val intent = Intent( activity , CorrectOrWrongActivity::class.java )
            intent.putExtra("data" , questionModel)
            startActivity(intent)
        }
    }
}