package com.rabumkai.thaidanceproject

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.rabumkai.thaidanceproject.model.RegisterModel
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.PropertyName
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.statusBarColor = ContextCompat.getColor(this, R.color.green)
        setContentView(R.layout.activity_register)
        if( !Hawk.isBuilt() ){
            Hawk.init(this).build()
        }

        btnRegister.setOnClickListener {
            if( isConnected() ){
                val checkUserName = checkUserName()
                val checkPassword = checkPassword()

                if( checkUserName && checkPassword )
                    register()
                else{
                    if( !checkUserName ){
                        Toast.makeText(this, "กรุณาระบุ user name", Toast.LENGTH_LONG).show()
                    } else if (!checkPassword){
                        Toast.makeText(this, "กรุณาระบุ password ให้ตรงกัน", Toast.LENGTH_LONG).show()
                    }
                }
            }
            else{
                Toast.makeText(this@RegisterActivity, "กรุณาต่อ internet", Toast.LENGTH_LONG)
                    .show()
            }
        }

        cvToolBar.setOnClickListener {
            finish()
        }

    }

    override fun onDestroy() {
        super.onDestroy()

        Hawk.destroy()
    }

    private fun checkUserName():Boolean{
        return edtUserName.text.toString().isNotEmpty()
    }

    private fun checkPassword():Boolean{
        return edtConfirmPassword.text.toString() == edtPassword.text.toString()
    }
    private fun register(){
        val registerModel = RegisterModel(
            edtUserName.text.toString(),
            edtPassword.text.toString(),
            edtClassRoom.text.toString(),
            edtName.text.toString(),
            edtLastName.text.toString(),
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        )

//        (@PropertyName("username") var username:String = "",
//        @PropertyName("password") var password:String = "",
//        @PropertyName("classroom")var classroom:String = "",
//        @PropertyName("name") var name:String = "",
//        @PropertyName("lastname") var lastname:String = "",
//        @PropertyName("after_study") var after_study:String = "",
//        @PropertyName("before_study") var before_study:String = "",
//        @PropertyName("correct_or_wrong") var correct_or_wrong:String = "",
//        @PropertyName("fill_word") var fill_word:String = "",
//        @PropertyName("key") var key:String = "",
//        @PropertyName("is_done_exercise") var is_done_exercise:String = "",
//        @PropertyName("is_done_exercise_fill_word") var is_done_exercise_fill_word:String = "")

        // Write a message to the database
        val database = Firebase.database
        val myRef = database.getReference("dev").child("member_list")
        myRef.push().setValue(registerModel, object : DatabaseReference.CompletionListener {
            override fun onComplete(error: DatabaseError?, ref: DatabaseReference) {
                if (error != null) {
                    Toast.makeText(this@RegisterActivity, "ลงทะเบียนไม่สำเร็จ", Toast.LENGTH_LONG)
                        .show()
                } else {
                    Toast.makeText(this@RegisterActivity, "ลงทะเบียนสำเร็จ", Toast.LENGTH_LONG)
                        .show()

                    registerModel.key = ref.key!!

                    val intent = Intent( this@RegisterActivity , TabActivity::class.java )
                    val data  = Gson().toJson( registerModel )
                    save("profile" , registerModel)
                    intent.putExtra("profile" , data)
                    startActivityMainMenuActivity(intent)
                }
            }
        })
    }

    private fun save(key:String , registerModel:RegisterModel){
        Hawk.put(key , registerModel)
    }

    private fun startActivityMainMenuActivity( intent:Intent ){
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }
}