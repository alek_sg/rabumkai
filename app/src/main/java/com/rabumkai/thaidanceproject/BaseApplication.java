package com.rabumkai.thaidanceproject;

import android.app.Application;
import com.rabumkai.thaidanceproject.di.component.AppComponent;
import com.rabumkai.thaidanceproject.di.component.DaggerAppComponent;
import com.rabumkai.thaidanceproject.di.modules.AppModule;
import com.orhanobut.hawk.Hawk;

import java.util.Objects;

/**
 * Created by rungrawee.w on 05/06/2016.
 */
public class BaseApplication extends Application{
//    private static BaseApplication instance = new BaseApplication();
    AppComponent mAppComponent;
    private boolean is_done_exercise_before_study = false;

    public boolean isDoneExerciseBeforeStudy() {
        return is_done_exercise_before_study;
    }

    public void setDoneExerciseBeforeStudy(boolean isDone) {
        this.is_done_exercise_before_study = isDone;
    }

    @Override
    public void onCreate() {
//        instance = this;
        super.onCreate();
        mAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();

//        if( isNetworkAvailable(this) != null ){
//            FirebaseDatabase.getInstance().setPersistenceEnabled(false);
//        }else{
//            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
//        }

        Hawk.init(this).build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }


//    public NetworkInfo isNetworkAvailable(Context context) {
//        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetworkInfo = Objects.requireNonNull(connectivityManager).getActiveNetworkInfo();
//
//        return activeNetworkInfo;
//    }


//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
//
//
//    }
//
//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//    }
//
//    @Override
//    public Context getApplicationContext() {
//        return super.getApplicationContext();
//    }
//
//
//
//    public static BaseApplication getInstance() {
//        return instance;
//    }
//
//    public static Context getContext(){
//        return instance;
//    }

//    @Override
//    public void onActivityCreated(Activity activity, Bundle bundle) {
//        Hawk.init(activity).build();
//    }
//
//    @Override
//    public void onActivityStarted(Activity activity) {
//
//    }
//
//    @Override
//    public void onActivityResumed(Activity activity) {
//
//    }
//
//    @Override
//    public void onActivityPaused(Activity activity) {
//
//    }
//
//    @Override
//    public void onActivityStopped(Activity activity) {
//
//    }
//
//    @Override
//    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
//
//    }
//
//    @Override
//    public void onActivityDestroyed(Activity activity) {
//
//    }
}
