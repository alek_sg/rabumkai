package com.rabumkai.thaidanceproject

import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.adapter.office.Extention.showCorrectQuestion
import com.adapter.office.Extention.showFailedQuestion
import com.bumptech.glide.Glide
import com.rabumkai.thaidanceproject.model.QuestionModel
import com.rabumkai.thaidanceproject.model.QuizModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_question.*
import kotlinx.android.synthetic.main.layout_posture.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class QuestionActivity : BaseActivity() {
    private var lstData:ArrayList<QuestionModel> ?= null
    private var lstQuestionModel:ArrayList<QuestionModel> ?= null
    private var currentCount = 0
    private var itemsAnswer = HashMap<String, String>()
    private var isBeforeStudy = false
    private var event = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question)

        lstQuestionModel = ArrayList<QuestionModel>()

        val bundle = intent.extras
        val data = bundle?.getString("data").toString()
        val before_study = bundle?.getString("before_study").toString()

        if( bundle!!.containsKey("event") )
            event = bundle?.getString("event").toString()

        isBeforeStudy = before_study == "true"

        val quizModel = Gson().fromJson(data, QuizModel::class.java)
        lstData = quizModel.content
        lstQuestionModel = getRandomQuestion()

        try {
            if( isBeforeStudy )
                tvTitleNavigator.text = "แบบทดสอบก่อนเรียน"
            else
                tvTitleNavigator.text = "แบบทดสอบหลังเรียน"

            btnCheckAnswer.isEnabled = false
            btnCheckAnswer.alpha = 0.2F
            llContainerChoose.isEnabled = true

            bindView(lstQuestionModel!![currentCount])
            bindEvent()
        }catch (ex:Exception){
            Log.e("","ex = "+ex.toString())
        }
    }

    private fun getAmountQuestion():Int{
        return if( isBeforeStudy ) 15
        else 15

//        return 3
    }

    private fun getRandomQuestion():ArrayList<QuestionModel>{

        val lstQuestionModel = ArrayList<QuestionModel>()
        while ( lstQuestionModel.size < getAmountQuestion() ){
            val ran = (0..(lstData!!.size-1)).random()
            lstQuestionModel.add(lstData!![ran])
            lstData!!.removeAt(ran)
        }
        return lstQuestionModel
    }

    private fun bindEvent(){

        llRootNavigator.setOnClickListener {
            finish()
        }
        btnNext.setOnClickListener {
            if( lstQuestionModel!!.size <= 0 ) return@setOnClickListener

            if( isLastPage() ){
                summaryScore()
            }else{
                if( currentCount < lstQuestionModel!!.size-1 ){
                    currentCount++

                    bindView(lstQuestionModel!![currentCount])
                }
            }

            btnCheckAnswer.isEnabled = false
            btnCheckAnswer.alpha = 0.2F

            llContainerChoose.isEnabled = true
        }

        btnPrevious.setOnClickListener {

            if( currentCount > 0 ){
                currentCount--

                bindView(lstQuestionModel!![currentCount])
            }
        }

        btnSubmit.setOnClickListener {
            summaryScore()
        }

        btnCheckAnswer.setOnClickListener {
            btnCheckAnswer.isEnabled = false
            btnCheckAnswer.alpha = 0.2F
            llContainerChoose.isEnabled = false

            if( itemsAnswer[lstQuestionModel!![currentCount].question] == lstQuestionModel!![currentCount].answer ){
                val dialog = displayAnswerCorrect(lstQuestionModel!![currentCount].answer,lstQuestionModel!![currentCount].question)
                dialog.window?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

                val btnNextQuestion = dialog.findViewById<Button>(R.id.btnNext)
                btnNextQuestion.setOnClickListener {
                    dialog.dismiss()

                    ivCheckAnswer.visibility = View.GONE
                    btnNext.performClick()
                }
                dialog.show()
            }else{
                if( isBeforeStudy ){
                    val dialog = displayAnswerFail(lstQuestionModel!![currentCount].question ,
                        lstQuestionModel!![currentCount].answer,
                        itemsAnswer[lstQuestionModel!![currentCount].question]!!)

                    dialog.window?.setLayout(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

                    val btnNextQuestion = dialog.findViewById<Button>(R.id.btnNext)
                    btnNextQuestion.setOnClickListener {
                        dialog.dismiss()

                        ivCheckAnswer.visibility = View.GONE
                        btnNext.performClick()
                    }
                    dialog.show()
                }
                else{
                    clearBackgroundChoose()

                    val dialog = displayAnswerFail(lstQuestionModel!![currentCount].question ,
                        lstQuestionModel!![currentCount].answer,
                        itemsAnswer[lstQuestionModel!![currentCount].question]!!)

                    dialog.window?.setLayout(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
                    val btnNextQuestion = dialog.findViewById<Button>(R.id.btnNext)
                    btnNextQuestion.setOnClickListener {
                        dialog.dismiss()

                        ivCheckAnswer.visibility = View.GONE
                        btnNext.performClick()
                    }
                    dialog.show()
                }
            }
        }
    }

    private fun displayAnswerCorrect(answerCorrect:String,question:String):Dialog{
//        val frameLayout = getTextViewChoose(answerCorrect!!)
//        val tvChooseSelected = frameLayout.findViewById<TextView>(R.id.tvChoose)
//        val ivCheckAnswer = frameLayout.findViewById<ImageView>(R.id.ivCheckAnswer)
//        val drawableCheckAnswer = resources.getDrawable(R.drawable.baseline_check_circle_outline_green_a700_48dp)
//        val drawableBGChooseSelect = resources.getDrawable(R.drawable.bg_choose_select)
//        ivCheckAnswer.visibility = View.VISIBLE
//
//        setBackgroundChoose(tvChooseSelected = tvChooseSelected ,
//            drawable = drawableBGChooseSelect, ivCheckAnswer = ivCheckAnswer, drawableCheckAnswer = drawableCheckAnswer
//        )

        return this.showCorrectQuestion(answerCorrect,question)
    }

    private fun displayAnswerFail(question:String , correctAnswer:String , myAnswer:String):Dialog{
//        val frameLayout = getTextViewChoose(myAnswer!!)
//        val tvChooseSelected = frameLayout.findViewById<TextView>(R.id.tvChoose)
//        val ivCheckAnswer = frameLayout.findViewById<ImageView>(R.id.ivCheckAnswer)
//        val drawableCheckAnswer = resources.getDrawable(R.drawable.baseline_highlight_off_red_700_48dp)
//        val drawableBGChooseSelect = resources.getDrawable(R.drawable.bg_choose_fail)
//        ivCheckAnswer.visibility = View.VISIBLE
//
//        setBackgroundChoose(tvChooseSelected = tvChooseSelected ,
//            drawable = drawableBGChooseSelect, ivCheckAnswer = ivCheckAnswer, drawableCheckAnswer = drawableCheckAnswer
//        )

//        showFailedQuestion( question:String , correctAnswer:String , myAnswer:String , isBeforeStudy:Boolean )
        return this.showFailedQuestion(question , correctAnswer , myAnswer, isBeforeStudy)
    }

    private fun isAnswerCorrect():Boolean{
        lstQuestionModel!![currentCount].apply {
            return if( itemsAnswer.containsKey(question) ){
                itemsAnswer[question] == answer
            }else{
                false
            }
        }

        return false
    }

    private fun summaryScore(){

        var score = 0
        for( i in 0 until lstQuestionModel!!.size ){
            if( itemsAnswer[lstQuestionModel!![i].question] == lstQuestionModel!![i].answer ){
                score++
            }
        }

        val dataQuestion = Gson().toJson(lstQuestionModel)

        val intent = Intent( this , SummaryScoreQuestionActivity::class.java )
        intent.putExtra("answer" , itemsAnswer)
        intent.putExtra("score" , score)
        intent.putExtra("total" , lstQuestionModel!!.size)
        intent.putExtra("list_question" , dataQuestion)
        if(isBeforeStudy){
            intent.putExtra("before_study" , "true")

            if( event.isNotEmpty() )
                intent.putExtra("chapter" , event)
        }
        else{
            intent.putExtra("before_study" , "false")
        }

        startActivity(intent)
        finish()
    }

    private fun getTextViewChoose(answerChoose:String):FrameLayout{
        for( i in 0 until llContainerChoose.childCount){
            val tvChoose = llContainerChoose.getChildAt(i).findViewById<TextView>(R.id.tvChoose)
            if( answerChoose == tvChoose.text.toString() ){
                return tvChoose.parent as FrameLayout
            }
        }

        return null!!
    }

    private fun createChoose(questionModel:QuestionModel ){
        llContainerChoose.removeAllViews()
        for( i in 0 until questionModel.choose!!.size  ){
            val view = layoutInflater.inflate(R.layout.item_choose , null)
            val tvChoose = view.findViewById<TextView>(R.id.tvChoose)
            val ivCheckAnswer = view.findViewById<ImageView>(R.id.ivCheckAnswer)

            ivCheckAnswer.visibility = View.GONE

            tvChoose.text = questionModel.choose!![i]
            tvChoose.background = resources.getDrawable(R.drawable.bg_choose)
            tvChoose.setTextColor(resources.getColor(R.color.blue_dark))
            tvChoose.setOnClickListener {
                view.tag = tvChoose.text.toString()
                itemsAnswer[questionModel.question] = tvChoose.text.toString()

                clearBackgroundChoose()
                setBackgroundChoose(tvChoose ,
                    resources.getDrawable(R.drawable.bg_choose_select) ,
                    ivCheckAnswer ,
                    resources.getDrawable(R.drawable.baseline_highlight_off_red_700_48dp) )

                btnCheckAnswer.isEnabled = true
                btnCheckAnswer.alpha = 1.0F
                llContainerChoose.isEnabled = true
            }

            val param = LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT , LinearLayout.LayoutParams.WRAP_CONTENT )
            param.topMargin = 20
            view.layoutParams = param

            llContainerChoose.addView(view)
        }
    }

    private fun isLastPage() = (currentCount == lstQuestionModel!!.size-1)
    private fun isFirstPage() = (currentCount == 0)

    private fun bindView( questionModel:QuestionModel  ){

        ivCheckAnswer.visibility = View.GONE
        tvQuestion.text = questionModel.question
        createChoose(questionModel)

        if( isFirstPage() )
            btnPrevious.visibility = View.INVISIBLE
        else
            btnPrevious.visibility = View.INVISIBLE

        if( isLastPage() ){
            btnSubmit.visibility = View.INVISIBLE
            btnNext.visibility = View.GONE
        }else{
            btnSubmit.visibility = View.GONE
            btnNext.visibility = View.INVISIBLE
        }

        if( itemsAnswer.size > 0 ){
            if( itemsAnswer.containsKey(questionModel.question) ){
                val answerChoose = itemsAnswer[questionModel.question]
                val frameLayout = getTextViewChoose(answerChoose!!)
                val tvChooseSelected = frameLayout.findViewById<TextView>(R.id.tvChoose)
                val ivCheckAnswer = frameLayout.findViewById<ImageView>(R.id.ivCheckAnswer)

//                setBackgroundChoose(tvChooseSelected = tvChooseSelected)
            }
        }

        tvCountQuestion.text = "ข้อที่ "+(currentCount+1).toString() + " / "+lstQuestionModel!!.size

        if( lstQuestionModel!![currentCount].image.isNotEmpty() ){
            val baseActivity = this as BaseActivity
            val resource = baseActivity.getResourceId(baseActivity ,
                lstQuestionModel!![currentCount].image ,
                "drawable" ,
                baseActivity.packageName)

            Glide.with(baseActivity).load(resource).into(ivImage)
            ivImage.visibility = View.VISIBLE
        }else{
            ivImage.visibility = View.GONE
        }

    }

    private fun clearBackgroundChoose(){
        for( i in 0 until llContainerChoose.childCount  ){
            val tvChoose = llContainerChoose.getChildAt(i).findViewById<TextView>(R.id.tvChoose)
            tvChoose.background = resources.getDrawable(R.drawable.bg_choose)
            tvChoose.setTextColor(resources.getColor(R.color.blue_dark))
        }
    }

    private fun setBackgroundChoose(tvChooseSelected:TextView ,
                                    drawable:Drawable = resources.getDrawable(R.drawable.bg_choose_select),
                                    ivCheckAnswer:ImageView ,
                                    drawableCheckAnswer:Drawable = resources.getDrawable(R.drawable.baseline_check_circle_outline_green_a700_48dp)){

        tvChooseSelected.background = drawable
        tvChooseSelected.setTextColor(resources.getColor(R.color.white))

        ivCheckAnswer.setImageDrawable(drawableCheckAnswer)
    }
}