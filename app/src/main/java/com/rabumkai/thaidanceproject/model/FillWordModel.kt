package com.rabumkai.thaidanceproject.model

data class QuestionFillWordModel(var content:ArrayList<FillWordModel> = arrayListOf(),
                     var sub_menu:String = "") {
}
data class FillWordModel(var question:String = "",
                         var answer:String = "",
                         var hint:String = "",
                         var final_answer:String = "") {
}