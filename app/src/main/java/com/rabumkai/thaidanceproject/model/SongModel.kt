package com.rabumkai.thaidanceproject.model


import com.google.gson.annotations.SerializedName



data class SongModel(
    @SerializedName("song")
    val song: List<Song> = arrayListOf()
)

data class Song(
    @SerializedName("content")
    val content: List<String> = arrayListOf(),
    @SerializedName("id")
    val id: String = "",
    @SerializedName("sub_menu")
    val sub_menu: String = ""
)