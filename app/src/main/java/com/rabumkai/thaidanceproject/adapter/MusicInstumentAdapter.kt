package com.rabumkai.thaidanceproject.adapter;

import adapter.com.extention.loadImage
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import com.bumptech.glide.Glide
import com.rabumkai.thaidanceproject.BaseActivity
import com.rabumkai.thaidanceproject.R
import com.rabumkai.thaidanceproject.model.MusicalInstrumentModel
import kotlinx.android.synthetic.main.layout_music_equipment.view.*
import java.lang.Exception


class MusicInstumentAdapter(val context: Context, val lstPostureActionModel: ArrayList<MusicalInstrumentModel>, val listener:ClickListener) : RecyclerView.Adapter<MusicInstumentAdapter.ViewHolder>() {

    interface ClickListener{
        fun onClickItem(model: MusicalInstrumentModel)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(lstPostureActionModel[position] )
        holder.itemView.setOnClickListener {
            listener.onClickItem(lstPostureActionModel[position])
        }
    }

    override fun getItemCount() = lstPostureActionModel.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return  ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_music_equipment , parent,false))
    }

    class ViewHolder(itemsView: View ): RecyclerView.ViewHolder(itemsView) {

        fun bind(item: MusicalInstrumentModel ) {
            itemView.apply {
                try {
                    val baseActivity = context as BaseActivity
                    val resource = baseActivity.getResourceId(context , item.url , "drawable" , baseActivity.packageName)
                    Glide.with(baseActivity).load(resource).into(ivMusicEquipment)
                    tvMusicEquipment.text = item.key
                } catch ( ex:Exception) {
                    Log.e("LitsRedeemAdapter" , "ex = "+ex.toString())
                }
            }
        }
    }
}