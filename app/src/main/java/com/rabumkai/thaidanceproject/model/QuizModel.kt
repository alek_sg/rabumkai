package com.rabumkai.thaidanceproject.model

data class QuizModel(var content:ArrayList<QuestionModel> = arrayListOf(),
                      var sub_menu:String = "") {
}
data class QuestionModel(var question:String = "",
                         var answer:String = "",
                         var image:String = "",
                         var hint:String = "",
                         var final_answer:String = "",
                         var choose:ArrayList<String> = arrayListOf()) {
}