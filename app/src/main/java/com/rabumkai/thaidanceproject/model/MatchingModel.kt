package com.rabumkai.thaidanceproject.model


import android.util.Log
import com.google.gson.annotations.SerializedName

data class MatchingModel(
    @SerializedName("img")
    var img: String = "",
    @SerializedName("posture_name")
    var postureName: String = "",
    var isRenderImg:Boolean = false,
    var isSelect:Boolean = false,
    var isRemove:Boolean = false,
    var id:Int = 0)
{
    override fun toString(): String {
        return "img->$img\npostureName->$postureName , isRenderImg-->$isRenderImg"
    }

    fun isEmpty():Boolean {
       return img.isEmpty() && postureName.isEmpty()
    }
}
