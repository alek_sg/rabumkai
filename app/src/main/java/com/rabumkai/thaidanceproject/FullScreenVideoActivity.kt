package com.rabumkai.thaidanceproject

import android.app.Activity
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.MediaController
import kotlinx.android.synthetic.main.activity_full_screen_video.*
import kotlinx.android.synthetic.main.activity_full_screen_video.ivFullScreen
import kotlinx.android.synthetic.main.fragment_tab1_song.*

class FullScreenVideoActivity : BaseActivity() {

    var requestCode = 0
    var idResource = 0
    var goto = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_video)

        Log.e("FullScreenVideoActivity","onCreate")

        var seek = 0


        val bundle = intent.extras
        if( bundle != null ){
            seek = bundle.getInt("seek")
            idResource = bundle.getInt("resource")
            requestCode = bundle.getInt("requestCode")
            if( bundle.containsKey("goto") ) {
                goto = bundle.getString("goto")!!
            }
        }else{
            seek = 6000
            idResource = R.raw.acting
        }

       bindVDO(seek)

        ivFullScreen.setOnClickListener {
            val intent = Intent()
            intent.putExtra("requestCode" , requestCode)
            intent.putExtra("seek" , vdoView.currentPosition)
            intent.putExtra("resource" , idResource)
            setResult(Activity.RESULT_OK , intent)
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if(vdoView != null){
            vdoView.stopPlayback()
            vdoView.setOnCompletionListener(null)
            vdoView.setOnPreparedListener(null)
        }
    }

    private fun bindVDO(seek:Int){
        val mediaController = MediaController(this)
        mediaController.setAnchorView(vdoViewTab1)
        vdoView.setMediaController(mediaController)
        playVdo(idResource,seek)
    }

    private fun playVdo(idResource:Int , seek:Int){
        try{
            val path = "android.resource://com.rabumkai.thaidanceproject/"+idResource
            val u = Uri.parse(path)
            vdoView!!.setVideoURI(u)
            vdoView!!.setOnPreparedListener(MediaPlayer.OnPreparedListener { mp ->
                mp.isLooping = false
                mp.setScreenOnWhilePlaying(false)
                vdoView!!.seekTo(seek);
                vdoView.start()

                vdoView.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
                    override fun onCompletion(mp: MediaPlayer?) {
                        if( requestCode <= 0 ){
                            if( goto.isNotEmpty() ){
                                if( goto == "goto_question" ){
                                    val returnIntent = Intent()
                                    returnIntent.putExtra("goto", goto)
                                    setResult(Activity.RESULT_OK, returnIntent)
                                    finish()
                                }else if( goto == "goto_history" ){
                                    finish()
                                }
                            }
                            else{
                                finish()
                            }
                        }
                    }
                })
            })
        }catch (e:Exception){
            Log.e("FullScreenVideoActivity","e-->"+e.toString())
        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("requestCode" , requestCode)
        intent.putExtra("seek" , vdoView.currentPosition)
        intent.putExtra("resource" , idResource)
        setResult(Activity.RESULT_OK , intent)
        finish()

        super.onBackPressed()
    }

    override fun onPause() {
        super.onPause()

        if( vdoView != null )
            vdoView!!.pause()
    }
    override fun onStop() {
        super.onStop()

        if( vdoView != null )
            vdoView!!.pause()
    }
}