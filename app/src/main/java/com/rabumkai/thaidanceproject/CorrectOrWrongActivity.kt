package com.rabumkai.thaidanceproject

import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.adapter.office.Extention.showAnswer
import com.rabumkai.thaidanceproject.model.CorrectOrWrongModel
import com.rabumkai.thaidanceproject.model.RootCorrectOrWrongModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_correct_or_wrong.*
import kotlinx.android.synthetic.main.activity_correct_or_wrong.btnCheckAnswer
import kotlinx.android.synthetic.main.activity_correct_or_wrong.llRootNavigator
import kotlinx.android.synthetic.main.activity_correct_or_wrong.tvCountQuestion
import kotlinx.android.synthetic.main.activity_correct_or_wrong.tvQuestion
import kotlinx.android.synthetic.main.activity_correct_or_wrong.tvTitleNavigator
import kotlinx.android.synthetic.main.activity_fill_word.*

class CorrectOrWrongActivity : BaseActivity() {

    private var lstData:ArrayList<CorrectOrWrongModel> ?= null
    private var lstQuestionModel:ArrayList<CorrectOrWrongModel> ?= null
    private var currentCount = 0
    private var itemsAnswer = HashMap<String, String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_correct_or_wrong)
        lstQuestionModel = ArrayList<CorrectOrWrongModel>()

        val bundle = intent.extras
        val data = bundle?.getString("data").toString()

        val quizModel = Gson().fromJson(data, RootCorrectOrWrongModel::class.java)
        lstData = quizModel.content
        lstQuestionModel = getRandomQuestion()

        tvTitleNavigator.text = "แบบฝึกหัดที่ 2"
        tvTitleNavigator.setTextColor(resources.getColor(R.color.white))

        try {
            bindView(lstQuestionModel!![currentCount])
            bindEvent()

            clearBackgroundChoose()
            btnCheckAnswer.alpha = 0.5F
            btnCheckAnswer.isEnabled = false
        }catch (ex:Exception){
            Log.e("","ex = "+ex.toString())
        }
    }

    private fun checkAnswer(){

        if( itemsAnswer[lstQuestionModel!![currentCount].question] == lstQuestionModel!![currentCount].answer ){
            displayAnswerCorrect(itemsAnswer[lstQuestionModel!![currentCount].question]!! , lstQuestionModel!![currentCount].question)
        }else{
            displayAnswerFail(lstQuestionModel!![currentCount] , itemsAnswer[lstQuestionModel!![currentCount].question]!!)
        }

        clearBackgroundChoose()
        btnCheckAnswer.alpha = 0.5F
        btnCheckAnswer.isEnabled = false

    }

    private fun displayAnswerCorrect(myAnswer:String , question:String){
        this.showAnswer().apply {
            window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            window?.setBackgroundDrawableResource(android.R.color.transparent)

            val llRootWrong = findViewById<LinearLayout>(R.id.llRootWrong)
            val tvQuestion = findViewById<TextView>(R.id.tvQuestion)
            val tvAnswer = findViewById<TextView>(R.id.tvAnswer)
            val llRootCorrect = findViewById<LinearLayout>(R.id.llContainerMyAnswer)
            val tvMyAnswerCorrect = findViewById<TextView>(R.id.tvMyAnswerCorrect)
            val tvQuestionCorrect = findViewById<TextView>(R.id.tvQuestionCorrect)

            llRootWrong.visibility = View.GONE
            llRootCorrect.visibility = View.VISIBLE

            window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            window?.setBackgroundDrawableResource(android.R.color.transparent)

            if( myAnswer == "0" )
                tvMyAnswerCorrect.text = "ไม่ใช่"
            else
                tvMyAnswerCorrect.text = "ใช่"

            tvQuestionCorrect.text = question

            val btnNext = findViewById<Button>(R.id.btnNext)
            btnNext.setOnClickListener {
                dismiss()

                currentCount++
                if(lstQuestionModel!!.size > currentCount)
                    bindView(lstQuestionModel!![currentCount])
                else
                    summaryScore()
            }
            show()
        }
    }

    private fun displayAnswerFail(correctOrWrongModel: CorrectOrWrongModel , myAnswer: String){

        this.showAnswer().apply {
            window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            window?.setBackgroundDrawableResource(android.R.color.transparent)

            val llRootWrong = findViewById<LinearLayout>(R.id.llRootWrong)
            val tvQuestion = findViewById<TextView>(R.id.tvQuestion)
            val tvMyAnswer = findViewById<TextView>(R.id.tvMyAnswer)
            val tvAnswer = findViewById<TextView>(R.id.tvAnswer)

            val llRootCorrect = findViewById<LinearLayout>(R.id.llContainerMyAnswer)

            tvQuestion.text = correctOrWrongModel.question
            if( correctOrWrongModel.answer == "0" )
                tvAnswer.text = "ไม่ใช่"
            else
                tvAnswer.text = "ใช่"

            if( myAnswer == "0" )
                tvMyAnswer.text = "ไม่ใช่"
            else
                tvMyAnswer.text = "ใช่"

            llRootWrong.visibility = View.VISIBLE
            llRootCorrect.visibility = View.GONE

            val btnNext = findViewById<Button>(R.id.btnNext)
            btnNext.setOnClickListener {
                dismiss()

                currentCount++
                if(lstQuestionModel!!.size > currentCount)
                    bindView(lstQuestionModel!![currentCount])
                else
                    summaryScore()
            }
            show()
        }
    }

    private fun bindEvent(){

        llRootNavigator.setOnClickListener {
            finish()
        }

        tvYes.setOnClickListener {
            itemsAnswer[lstQuestionModel!![currentCount].question] = "1"

            clearBackgroundChoose()
            setBackgroundChoose(tvYes , drawable = resources.getDrawable(R.drawable.bg_choose_select))

            btnCheckAnswer.alpha = 1.0F
            btnCheckAnswer.isEnabled = true
        }

        tvNo.setOnClickListener {
            itemsAnswer[lstQuestionModel!![currentCount].question] = "0"

            clearBackgroundChoose()
            setBackgroundChoose(tvNo , drawable = resources.getDrawable(R.drawable.bg_choose_select))

            btnCheckAnswer.alpha = 1.0F
            btnCheckAnswer.isEnabled = true
        }

        btnCheckAnswer.setOnClickListener {
            checkAnswer()
        }

        llRootNavigator.setOnClickListener {
            finish()
        }
    }

    private fun clearBackgroundChoose(){
        tvNo.background = resources.getDrawable(R.drawable.bg_choose)
        tvNo.setTextColor(resources.getColor(android.R.color.black))

        tvYes.background = resources.getDrawable(R.drawable.bg_choose)
        tvYes.setTextColor(resources.getColor(android.R.color.black))
    }

    private fun setBackgroundChoose(tvChooseSelected:TextView,
                                    drawable: Drawable = resources.getDrawable(R.drawable.bg_choose_select)){

        tvChooseSelected.background = drawable
        tvChooseSelected.setTextColor(resources.getColor(R.color.white))
    }

    private fun bindView( questionModel: CorrectOrWrongModel){
        if( isLastPage() ){
           summaryScore()
        }else{
//            ivCorrect.alpha = 1.0F
//            ivCorrect.isEnabled = true
//
//            ivWrong.alpha = 1.0F
//            ivWrong.isEnabled = true

            tvQuestion.text = questionModel.question
            tvCountQuestion.text = "ข้อที่ "+(currentCount+1).toString() + " / "+lstQuestionModel!!.size
        }
    }

    private fun summaryScore(){

        var score = 0
        for( i in 0 until lstQuestionModel!!.size ){
            if( itemsAnswer[lstQuestionModel!![i].question] == lstQuestionModel!![i].answer ){
                score++
            }
        }
        val data = Gson().toJson(lstQuestionModel)
        val intent = Intent( this , SummaryScoreQuestionActivity::class.java )
        intent.putExtra("score" , score)
        intent.putExtra("total" , lstQuestionModel!!.size)
        intent.putExtra("correct_or_wrong" , true)
        intent.putExtra("answer",itemsAnswer)
        intent.putExtra("list_correct_or_wrong",data)

        startActivity(intent)
        finish()
    }

    private fun isLastPage() = (currentCount == lstQuestionModel!!.size)
    private fun isFirstPage() = (currentCount == 0)

    private fun getRandomQuestion():ArrayList<CorrectOrWrongModel>{

        val lstQuestionModel = ArrayList<CorrectOrWrongModel>()

        while ( lstQuestionModel.size < 10 ){
            val ran = (0..(lstData!!.size-1)).random()
            lstQuestionModel.add(lstData!![ran])
            lstData!!.removeAt(ran)
        }
        return lstQuestionModel
    }
}