package com.adapter.office.Extention

import java.security.MessageDigest
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    val digested = md.digest(toByteArray())
    return digested.joinToString("") {
        String.format("%02x", it)
    }
}

fun String.numberFormat(): String {
    try{
        val dec = DecimalFormat("#,###")
        return dec.format(this.toInt())
    }catch (ex: Exception){
        return this
    }
}

fun String.getGUID():String{
    return UUID.randomUUID().toString()
}

fun String.getConvertDate():String{

    try{
        val inputFormat = SimpleDateFormat("yyyy-MM-dd");
        val formatter = SimpleDateFormat("d MMMM yyyy", Locale("th", "TH"))
//    val formatter = SimpleDateFormat("EEEE ที่ dd เดือน MMMM พ.ศ. yyyy", Locale("th", "TH"))

        val dateInput = inputFormat.parse(this)
        var convert = formatter.format(dateInput);
        val date =  convert.split(" ")
        return date[0]+" "+date[1]+" "+(date[2].toInt()+543)
    }catch (ex:Exception){
        return this
    }
}
fun String.formatDateShot():String{
    try{
        val inputFormat = SimpleDateFormat("yyyy-MM-dd");
        val formatter = SimpleDateFormat("d MMM yyyy", Locale("th", "TH"))
//    val formatter = SimpleDateFormat("EEEE ที่ dd เดือน MMMM พ.ศ. yyyy", Locale("th", "TH"))

        val dateInput = inputFormat.parse(this)
        var convert = formatter.format(dateInput);
        val date =  convert.split(" ")
        val yearShot = (date[2].toInt()+543)
        convert = date[0]+" "+date[1]+" "+ yearShot.toString().substring(2,4)
        return convert
    }catch (ex:Exception){
        return this
    }
}

fun String.formatDateTimeHistoryAuction():String{
    try{
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        val formatter = SimpleDateFormat("d MMM HH:mm:ss", Locale("th", "TH"))
//    val formatter = SimpleDateFormat("EEEE ที่ dd เดือน MMMM พ.ศ. yyyy", Locale("th", "TH"))

        val dateInput = inputFormat.parse(this)
        var convert = formatter.format(dateInput);
//        val date =  convert.split(" ")
//        val yearShot = (date[2].toInt()+543)
//        convert = date[0]+" "+date[1]+" "+ yearShot.toString().substring(2,4)
        return convert
    }catch (ex:Exception){
        return this
    }
}

fun String.formatMonthShot():String{
    try{
        val inputFormat = SimpleDateFormat("yyyy-MM-dd");
        val formatter = SimpleDateFormat("d MMM yyyy", Locale("th", "TH"))
//    val formatter = SimpleDateFormat("EEEE ที่ dd เดือน MMMM พ.ศ. yyyy", Locale("th", "TH"))

        val dateInput = inputFormat.parse(this)
        var convert = formatter.format(dateInput);
        val date =  convert.split(" ")
        val yearShot = (date[2].toInt()+543)
        convert = date[0]+" "+date[1]+" "+ yearShot.toString()
        return convert
    }catch (ex:Exception){
        return this
    }
}

fun String.formatDateTime(): Date? {
    try{
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm") //ถ้าต้องการให้เป็น 24( 13น. 14น. ตี1 ตี2 ) ชม ให้ set เป็น HH:mm แต่ถ้าต้องการระบบ AM PM ให้ set เป็น hh:mm
        return inputFormat.parse(this)
    }catch (ex:Exception){
        return null
    }
}

fun String.formatDateTime(pattern:String): Date? {
    try{
        val inputFormat = SimpleDateFormat(pattern) //ถ้าต้องการให้เป็น 24( 13น. 14น. ตี1 ตี2 ) ชม ให้ set เป็น HH:mm แต่ถ้าต้องการระบบ AM PM ให้ set เป็น hh:mm
        return inputFormat.parse(this)
    }catch (ex:Exception){
        return null
    }
}

fun String.countdownAuction():String{
//    var totalTime = (this.toLong() / 1000).toInt()
//    var hour = 0
//    var minute = 0
//    var second = 0
//    var days = 0

    val seconds: Long = this.toLong() / 1000
    val minutes = seconds / 60
    val hours = minutes / 60
    var days = hours / 24
    val totalHours = hours % 24
    val totalMinutes = minutes % 60
    val totalSecond = seconds % 60

    val time =
        days.toString() + ":" + hours % 24 + ":" + minutes % 60 + ":" + seconds % 60

    val sb = StringBuilder()
    sb.clear()

    if( days > 0 )
        sb.append(days).append(" วัน ")

    if (totalHours < 10) {
        sb.append("0").append(totalHours).append(":")
    } else {
        sb.append(totalHours).append(":")
    }
    if (totalMinutes < 10) {
        sb.append("0").append(totalMinutes).append(":")
    } else {
        sb.append(totalMinutes).append(":")
    }
    if (totalSecond < 10 ) {
        if( totalSecond >= 0 ){
            sb.append("0").append(totalSecond)
        }
        else{
            sb.append("00")
        }
    } else {
        sb.append(totalSecond)
    }
    return sb.toString()
}