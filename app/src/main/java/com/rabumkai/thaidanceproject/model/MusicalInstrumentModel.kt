package com.rabumkai.thaidanceproject.model

import com.google.firebase.database.DataSnapshot
import com.google.gson.annotations.SerializedName

data class MusicalInstrumentModel(var id:String = "",
                                  var url:String = "",
                                  var key: String = "") {
}