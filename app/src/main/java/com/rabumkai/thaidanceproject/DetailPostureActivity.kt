package com.rabumkai.thaidanceproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.rabumkai.thaidanceproject.adapter.PostureAdapter
import com.rabumkai.thaidanceproject.model.Content
import com.rabumkai.thaidanceproject.model.PostureX
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.item_posture.*
import java.util.ArrayList

class DetailPostureActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_posture)

        val bundle = intent.extras
        if( bundle == null ){
            Toast.makeText(this , "Something wrong" , Toast.LENGTH_SHORT).show()
            finish()
        }else{
            val data = bundle.getString("data")

            val lstType = object : TypeToken<ArrayList<Content?>?>() {}.type
            val lstContent: ArrayList<Content> = Gson().fromJson(data, lstType)



//            val postureModel = Gson().fromJson(posture , PostureActionModel::class.java)
//            bindView(postureModel)
        }
    }

    private fun bindView(lstPostureAction:ArrayList<Content>  ){
        val adapter = PostureAdapter(this, lstPostureAction , object : PostureAdapter.ClickListener {
            override fun onClickItem(postureActionModel: Content) {
                val data = Gson().toJson(postureActionModel)
                val i = Intent( this@DetailPostureActivity , DetailPostureActivity::class.java )
                i.putExtra("posture" , data )
                startActivity(i)
            }
        })

//        val dividerItemDecoration = DividerItemDecoration(
//            recyclerView.context,
//            LinearLayoutManager.VERTICAL
//        )
//        recyclerView.addItemDecoration(dividerItemDecoration)
//        recyclerView.layoutManager = LinearLayoutManager( this , LinearLayoutManager.VERTICAL , false )
//        recyclerView.adapter = adapter
    }
}