package com.rabumkai.thaidanceproject.model


import com.google.gson.annotations.SerializedName

data class Posture(
    @SerializedName("posture")
    val posture: List<PostureX>
)

data class PostureX(
    @SerializedName("content")
    val content: List<Content>  = arrayListOf(),
    @SerializedName("posture_mean")
    val posture_mean: String = "",
    @SerializedName("sub_menu")
    val sub_menu: String = "",
    @SerializedName("img")
    val img: String = ""
)

data class ContentMenu(
    @SerializedName("img")
    val img: String = "",
    @SerializedName("posture_name")
    val posture_name: String = ""
)

data class Content(
    @SerializedName("content_menu")
    var content_menu: List<ContentMenu>  = arrayListOf(),
    @SerializedName("img")
    var img: String = "",
    @SerializedName("menu")
    var menu: String = "",
    @SerializedName("posture_name")
    var posture_name: String = ""
)