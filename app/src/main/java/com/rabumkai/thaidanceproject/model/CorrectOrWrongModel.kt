package com.rabumkai.thaidanceproject.model

data class RootCorrectOrWrongModel(var content:ArrayList<CorrectOrWrongModel> = arrayListOf() ,
                                   var sub_menu:String = "") {
}

data class CorrectOrWrongModel(var answer:String = "",
                               var question:String = "") {
} 