package com.rabumkai.thaidanceproject

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.orhanobut.hawk.Hawk
import com.rabumkai.thaidanceproject.model.CorrectOrWrongModel
import com.rabumkai.thaidanceproject.model.FillWordModel
import com.rabumkai.thaidanceproject.model.QuestionModel
import com.rabumkai.thaidanceproject.model.RegisterModel
import kotlinx.android.synthetic.main.activity_summary_score_question.*

class SummaryScoreQuestionActivity : BaseActivity() {
    private val BEFORE_STUDY = 2
    private val AFTER_STUDY = 3
    private val FILL_WORD = 4
    private val CORRECT_OR_WRONG = 1

    private var isBeforeStudy = false
    private var correct_or_wrong = false
    private var fill_word = false
    private var isGameMatching = false

    private var fromChapter = ""

    private var mode = 0

    private var score = 0
    private var itemsAnswer = HashMap<String, String>()
    private var lstQuestionModel:ArrayList<QuestionModel> ?= null
    private var lstFillWordModel:ArrayList<FillWordModel> ?= null
    private var lstCorrectOrWrongModel:ArrayList<CorrectOrWrongModel> ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary_score_question)

        val bundle = intent.extras
        score = bundle?.getInt("score")!!
        val total = bundle.getInt("total")!!

        if( bundle.containsKey("chapter") )
            fromChapter = bundle.getString("chapter")!!

        if( bundle.containsKey("correct_or_wrong") ){
            correct_or_wrong = bundle.getBoolean("correct_or_wrong")
            mode = CORRECT_OR_WRONG

            val dataQuestion = bundle?.getString("list_correct_or_wrong").toString()

            lstCorrectOrWrongModel = Gson().fromJson(
                dataQuestion,
                object : TypeToken<List<CorrectOrWrongModel>?>() {}.type
            )

            tvTitleNavigator.text = "แบบฝีกหัดถูกผิด"
        }

        if( bundle.containsKey("before_study") ){
            val before_study = bundle?.getString("before_study").toString()

            isBeforeStudy = before_study == "true"

            if( isBeforeStudy ) tvTitleNavigator.text = "แบบทดสอบก่อนเรียน"
            else tvTitleNavigator.text = "แบบทดสอบหลังเรียน"

            if( isBeforeStudy ){
                if( fromChapter.isNotEmpty() ){
                    (this.application as BaseApplication).isDoneExerciseBeforeStudy = true
                }

                mode = BEFORE_STUDY
            }else{
                mode = AFTER_STUDY
            }
        }

        if( bundle.containsKey("list_question") ){
            val dataQuestion = bundle?.getString("list_question").toString()

            lstQuestionModel = Gson().fromJson(
                dataQuestion,
                object : TypeToken<List<QuestionModel>?>() {}.type
            )
        }

        if( bundle.containsKey("fill_word") ){
            fill_word = bundle.getBoolean("fill_word")

            val dataFillWord = bundle?.getString("list_fill_word").toString()

            lstFillWordModel = Gson().fromJson(
                dataFillWord,
                object : TypeToken<List<FillWordModel>?>() {}.type
            )
            mode = FILL_WORD

            tvTitleNavigator.text = "แบบฝีกหัดเติมคำ"
        }

        if( bundle.containsKey("answer") ){
            itemsAnswer = bundle.getSerializable("answer") as HashMap<String, String>
        }

        Glide.with(this)
            .load(R.drawable.banner_head)
            .into(ivBannerHeader)

        if( !Hawk.isBuilt() ){
            Hawk.init(this).build()
        }

        if( bundle.containsKey("game_matching") ){
            isGameMatching = true
            tvTitleNavigator.text = bundle.getString("game_matching")
            bindView(score, total , countIncorrect = bundle.getInt("count_not_matching"))
        }else if( bundle.containsKey("game_crossword") ){
            tvTitleNavigator.text = bundle.getString("game_crossword")
            bindView(score, total)
        }else if( bundle.containsKey("game_jigsaw") ){
            tvTitleWrong.text = "จำนวนต่ำแหน่งที่ผิด"
            tvTitleCorrect.text = "จำนวนต่ำแหน่งที่ถูก"
            isGameMatching = true
            tvTitleNavigator.text = bundle.getString("game_jigsaw")
            bindView(score, total , countIncorrect = bundle.getInt("count_not_matching"))
        }
        else{
            isGameMatching = false
            bindView(score, total)
            bindHistory()
        }

        if( isConnected() ){
            btnRetry.visibility = View.GONE
            pushScore()
        }
        else{
            btnRetry.visibility = View.VISIBLE
            Toast.makeText(
                this@SummaryScoreQuestionActivity,
                "กรุณาต่อ internet",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        Hawk.destroy()
    }

    private fun bindView(score: Int, total: Int , countIncorrect:Int = 0){
        if( isGameMatching ){
            cvContainerGameMatching.visibility = View.VISIBLE
            tvCountCorrect.text = score.toString()
            tvCountIncorrect.text = countIncorrect.toString()
        }else{
            cvContainerGameMatching.visibility = View.GONE
            tvCountCorrect.text = score.toString()
            tvCountIncorrect.text = (total-score).toString()
        }

        tvResultQuestion.text = "$score/$total"

        btnRetry.setOnClickListener {
            if( isConnected() ){
                btnRetry.visibility = View.GONE
                pushScore()
            }
            else{
                btnRetry.visibility = View.VISIBLE
                Toast.makeText(
                    this@SummaryScoreQuestionActivity,
                    "กรุณาต่อ internet",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        val btnMainMenu = findViewById<Button>(R.id.btnMainMenu)
        btnMainMenu.setOnClickListener {
            val intent = Intent(this@SummaryScoreQuestionActivity, TabActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            when (fromChapter) {
                "after_play_vdo_main_menu_end" -> intent.putExtra("event" , "go_to_history_activity")
                "event_form_after_song_activity" -> intent.putExtra("event" , "go_to_acting_activity")
                else -> {
                    intent.putExtra("event" , "go_to_main_menu")
                }
            }

            Log.e("","BaseApplication().is_done_exercise_before_study-->"+(this.application as BaseApplication).isDoneExerciseBeforeStudy)
            startActivity(intent)
            finish()
        }

        when (fromChapter) {
            "after_play_vdo_main_menu_end" -> {
                //ถ้ามากจากเมนูเพลง
                btnMainMenu.text = "บทเรียนถัดไป"// go to actingActivity
            }
            "event_form_after_song_activity" -> {
                //ถ้ามากจากเมนูเพลง
                btnMainMenu.text = "บทเรียนถัดไป"// go to actingActivity
            }
            else -> {
                //ถ้าไม่ใช่
                btnMainMenu.text = "กลับสู่เมนูหลัก"
            }
        }

        val llRootNavigator = findViewById<LinearLayout>(R.id.llRootNavigator)
        llRootNavigator.setOnClickListener {
            val intent = Intent(this@SummaryScoreQuestionActivity, TabActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }
    }

    private fun bindHistory(){
        if( itemsAnswer.size <= 0 ) return

        if( correct_or_wrong ){
            bindHistoryCorrectOrWrong()
        }else{
            if( fill_word ){
                bindHistoryFillWordAnswer()
            }else{
                bindHistoryAnswer()
            }
        }
    }

    private fun bindHistoryCorrectOrWrong(){
        for( i in lstCorrectOrWrongModel!!.indices ){
            var vHistoryFillWord = View(this)
            lstCorrectOrWrongModel!![i].apply {
                if( itemsAnswer.containsKey(question)){
                    vHistoryFillWord = if( itemsAnswer[question] == answer ){
                        displayAnswerCorrect(question , answer)
                    }else{
                        displayAnswerFailed(question , itemsAnswer[question]!! , answer )
                    }
                    llRootAnswer.addView(vHistoryFillWord)
                }
            }
        }
    }

    private fun bindHistoryFillWordAnswer(){
        for( i in lstFillWordModel!!.indices ){
            var vHistoryFillWord = View(this)
            lstFillWordModel!![i].apply {
                if( itemsAnswer.containsKey(question)){
                    var myanswer = itemsAnswer[question]
                    myanswer = myanswer?.replace("\\s".toRegex(), "")
                    if( myanswer == final_answer ){
                        vHistoryFillWord = displayAnswerCorrectFillWord(question , myanswer)
                    }else{
                        vHistoryFillWord = displayAnswerFailedFillWord(question , itemsAnswer[question]!! , final_answer )
                    }

                    llRootAnswer.addView(vHistoryFillWord)
                }
            }
        }
    }

    private fun bindHistoryAnswer(){

        for( i in lstQuestionModel!!.indices ){
            val vItemQuestion = layoutInflater.inflate(R.layout.item_question, null)
            val tvQuestion = vItemQuestion.findViewById<TextView>(R.id.tvQuestion)
            val ivImage = vItemQuestion.findViewById<ImageView>(R.id.ivImage)
            val llContainerChoose = vItemQuestion.findViewById<LinearLayout>(R.id.llRootChoose)
            tvQuestion.text = (i+1).toString()+"."+lstQuestionModel!![i].question

            if( lstQuestionModel!![i].image.isNotEmpty() ){
                val baseActivity = this as BaseActivity
                val resource = baseActivity.getResourceId(baseActivity ,
                    lstQuestionModel!![i].image ,
                    "drawable" ,
                    baseActivity.packageName)

                Glide.with(baseActivity).load(resource).into(ivImage)
            }else{
                ivImage.visibility = View.GONE
            }

            val questionModel = lstQuestionModel!![i]
            for( n in 0 until questionModel.choose.size  ){
                val view = layoutInflater.inflate(R.layout.item_choose , null)
                val tvChoose = view.findViewById<TextView>(R.id.tvChoose)
                val ivCheckAnswer = view.findViewById<ImageView>(R.id.ivCheckAnswer)

                ivCheckAnswer.visibility = View.GONE

                tvChoose.text = questionModel.choose!![n]
                tvChoose.background = resources.getDrawable(R.drawable.bg_choose)
                tvChoose.setTextColor(resources.getColor(R.color.blue_dark))

                val param = LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT , LinearLayout.LayoutParams.WRAP_CONTENT )
                param.topMargin = 20
                view.layoutParams = param

                llContainerChoose.addView(view)
            }

            if( itemsAnswer.containsKey(questionModel.question) ){
                if( itemsAnswer[questionModel.question] == questionModel.answer ){
                    displayAnswerCorrect(llContainerChoose , itemsAnswer[questionModel.question]!!)
                }else{
                    displayAnswerFail(llContainerChoose , itemsAnswer[questionModel.question]!! , questionModel.answer)
                    try{
                        displayAnswerCorrect(llContainerChoose , questionModel.answer)
                    }catch (ex:Exception){
                        Log.e ("","ex = "+ex.toString())
                    }
                }
            }

            llRootAnswer.addView(vItemQuestion)
        }
    }

    private fun pushScore(){
        val database = Firebase.database
        val myRef = database.getReference("dev")
        val registerModel = Hawk.get<RegisterModel>("profile")

        if( registerModel == null ) return

        val childUpdates: MutableMap<String, Any> = HashMap()
//        if( isGameMatching ){
//            //to do implement
//        }else{
//            if( correct_or_wrong ){
//                childUpdates["/member_list/" + registerModel.key + "/" + "correct_or_wrong"] = ""+score
//                registerModel.correct_or_wrong = ""+score
//            }
//            else{
//                if( isBeforeStudy ){
//                    childUpdates["/member_list/" + registerModel.key + "/" + "before_study"] = ""+score
//                    registerModel.before_study = ""+score
//                }
//                else if(fill_word){
//                    childUpdates["/member_list/" + registerModel.key + "/" + "fill_word"] = ""+score
//                    registerModel.fill_word = ""+score
//                }
//            }
//        }

        when (mode) {
            BEFORE_STUDY -> {
                childUpdates["/member_list/" + registerModel.key + "/" + "before_study"] = score.toString()
                if( fromChapter.isNotEmpty() )
                    childUpdates["/member_list/" + registerModel.key + "/" + "is_done_exercise"] = "true"

                registerModel.before_study = score.toString()
                registerModel.is_done_exercise = "true"
            }
            AFTER_STUDY -> {
                childUpdates["/member_list/" + registerModel.key + "/" + "after_study"] = score.toString()
                registerModel.after_study = score.toString()
            }
            CORRECT_OR_WRONG -> {
                childUpdates["/member_list/" + registerModel.key + "/" + "correct_or_wrong"] = score.toString()
                registerModel.correct_or_wrong = score.toString()
            }
            FILL_WORD -> {
                childUpdates["/member_list/" + registerModel.key + "/" + "fill_word"] = score.toString()
                if( fromChapter.isNotEmpty() )
                    childUpdates["/member_list/" + registerModel.key + "/" + "is_done_exercise_fill_word"] = "true"

                registerModel.fill_word = score.toString()
            }
        }


        Hawk.put("profile", registerModel)
        myRef.updateChildren(childUpdates, object : DatabaseReference.CompletionListener {
            override fun onComplete(error: DatabaseError?, ref: DatabaseReference) {
                if (error != null) {
                    Toast.makeText(
                        this@SummaryScoreQuestionActivity,
                        "ไม่สำเร็จ",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    Toast.makeText(
                        this@SummaryScoreQuestionActivity,
                        "สำเร็จ",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        })
    }

    private fun setBackgroundChoose(tvChooseSelected:TextView,
                                    drawable: Drawable = resources.getDrawable(R.drawable.bg_choose_select),
                                    ivCheckAnswer:ImageView,
                                    drawableCheckAnswer: Drawable = resources.getDrawable(R.drawable.baseline_check_circle_outline_green_a700_48dp)){

        tvChooseSelected.background = drawable
        tvChooseSelected.setTextColor(resources.getColor(R.color.white))

        ivCheckAnswer.setImageDrawable(drawableCheckAnswer)
    }

    private fun getTextViewChoose(llContainerChoose:LinearLayout , answerChoose:String): FrameLayout {
        for( i in 0 until llContainerChoose.childCount){
            val tvChoose = llContainerChoose.getChildAt(i).findViewById<TextView>(R.id.tvChoose)
            if( answerChoose == tvChoose.text.toString() ){
                return tvChoose.parent as FrameLayout
            }
        }

        return null!!
    }

    private fun displayAnswerCorrect(llContainerChoose:LinearLayout , answerCorrect:String) {
        val frameLayout = getTextViewChoose(llContainerChoose , answerCorrect!!)
        val tvChooseSelected = frameLayout.findViewById<TextView>(R.id.tvChoose)
        val ivCheckAnswer = frameLayout.findViewById<ImageView>(R.id.ivCheckAnswer)
        val drawableCheckAnswer = resources.getDrawable(R.drawable.baseline_check_circle_outline_green_a700_48dp)
        val drawableBGChooseSelect = resources.getDrawable(R.drawable.bg_choose_select)
        ivCheckAnswer.visibility = View.VISIBLE

        setBackgroundChoose(tvChooseSelected = tvChooseSelected ,
            drawable = drawableBGChooseSelect, ivCheckAnswer = ivCheckAnswer, drawableCheckAnswer = drawableCheckAnswer
        )

    }

    private fun displayAnswerFail(llContainerChoose:LinearLayout , myAnswer:String , correctAnswer:String) {
        val frameLayout = getTextViewChoose(llContainerChoose , myAnswer!!)
        val tvChooseSelected = frameLayout.findViewById<TextView>(R.id.tvChoose)
        val ivCheckAnswer = frameLayout.findViewById<ImageView>(R.id.ivCheckAnswer)
        val drawableCheckAnswer = resources.getDrawable(R.drawable.baseline_highlight_off_red_700_48dp)
        val drawableBGChooseSelect = resources.getDrawable(R.drawable.bg_choose_fail)
        ivCheckAnswer.visibility = View.VISIBLE

        setBackgroundChoose(tvChooseSelected = tvChooseSelected ,
            drawable = drawableBGChooseSelect, ivCheckAnswer = ivCheckAnswer, drawableCheckAnswer = drawableCheckAnswer
        )
    }

    private fun displayAnswerCorrectFillWord(question:String , myAnswer:String):View {
        val vLayoutHistory = layoutInflater.inflate(R.layout.layout_history_fill_word_correct, null)
        val tvQuestion = vLayoutHistory.findViewById<TextView>(R.id.tvQuestion)
        val tvMyAnswer = vLayoutHistory.findViewById<TextView>(R.id.tvMyAnswer)

        tvQuestion.text = question
        tvMyAnswer.text = myAnswer

        return vLayoutHistory
    }

    private fun displayAnswerFailedFillWord(question:String , myAnswer:String , answer:String):View {
        val vLayoutHistory = layoutInflater.inflate(R.layout.layout_history_fill_word_failed, null)
        val tvQuestion = vLayoutHistory.findViewById<TextView>(R.id.tvQuestion)
        val tvMyAnswer = vLayoutHistory.findViewById<TextView>(R.id.tvMyAnswer)
        val tvAnswer = vLayoutHistory.findViewById<TextView>(R.id.tvAnswer)

        tvQuestion.text = question
        tvMyAnswer.text = myAnswer
        tvAnswer.text = answer

        return vLayoutHistory
    }

    private fun displayAnswerCorrect(question:String , myAnswer:String):View {
        val vLayoutHistory = layoutInflater.inflate(R.layout.layout_history_fill_word_correct, null)
        val tvQuestion = vLayoutHistory.findViewById<TextView>(R.id.tvQuestion)
        val tvMyAnswer = vLayoutHistory.findViewById<TextView>(R.id.tvMyAnswer)

        tvQuestion.text = question
        tvMyAnswer.text = myAnswer

        if( myAnswer == "0" )
            tvMyAnswer.text = "ไม่ใช่"
        else
            tvMyAnswer.text = "ใช่"

        return vLayoutHistory
    }

    private fun displayAnswerFailed(question:String , myAnswer:String , answer:String):View {
        val vLayoutHistory = layoutInflater.inflate(R.layout.layout_history_fill_word_failed, null)
        val tvQuestion = vLayoutHistory.findViewById<TextView>(R.id.tvQuestion)
        val tvMyAnswer = vLayoutHistory.findViewById<TextView>(R.id.tvMyAnswer)
        val tvAnswer = vLayoutHistory.findViewById<TextView>(R.id.tvAnswer)

        tvQuestion.text = question
        if( myAnswer == "0" )
            tvMyAnswer.text = "ไม่ใช่"
        else
            tvMyAnswer.text = "ใช่"

        if( answer == "0" )
            tvAnswer.text = "ไม่ใช่"
        else
            tvAnswer.text = "ใช่"

        return vLayoutHistory
    }

    override fun onBackPressed() {
        super.onBackPressed()

        val intent = Intent(this@SummaryScoreQuestionActivity, TabActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }
}