package com.rabumkai.thaidanceproject

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.adapter.office.Extention.showDialogWarning
import com.adapter.office.Extention.showFailedQuestion
import com.adapter.office.Extention.showMatch
import com.adapter.office.Extention.showMissMatch
import com.rabumkai.thaidanceproject.adapter.GameMatchingAdapter
import com.rabumkai.thaidanceproject.model.MatchingModel
import kotlinx.android.synthetic.main.activity_fill_word.*
import kotlinx.android.synthetic.main.activity_game_matching.*
import kotlinx.android.synthetic.main.activity_game_matching.llRootNavigator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class GameMatchingActivity : BaseActivity() {
    private var firstMatchingModel = MatchingModel()
    private var secondMatchingModel = MatchingModel()
    private var gameMatchingAdapter:GameMatchingAdapter? = null
    private val lstMatchingModel = ArrayList<MatchingModel>()
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.Main)

    private var countMatchingCard = 0
    private var countWrong = 0
    private var matchingCardMax = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        setContentView(R.layout.activity_game_matching)

        bindView()

        btnGen.setOnClickListener {
            bindView()
        }

        llRootNavigator.setOnClickListener {
            val dlgWarning = this.showDialogWarning("จะออกจากเกมตอนนี้เหรอ?" , "ออกเลย")
            dlgWarning.window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            dlgWarning.window?.setBackgroundDrawableResource(android.R.color.transparent);

            val tvPositive = dlgWarning.findViewById<TextView>(R.id.tvPositive)
            tvPositive.setOnClickListener {
                dlgWarning.dismiss()
                finish()
            }
            val tvNegative = dlgWarning.findViewById<TextView>(R.id.tvNegative)
            tvNegative.setOnClickListener {
                dlgWarning.dismiss()
            }

            dlgWarning.show()
        }

        llRestartGame.setOnClickListener {
            val dlgWarning = this.showDialogWarning("เริ่มเล่นเกมใหม่อีกครั้ง?" , "เริ่มใหม่")
            dlgWarning.window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            dlgWarning.window?.setBackgroundDrawableResource(android.R.color.transparent);

            val tvPositive = dlgWarning.findViewById<TextView>(R.id.tvPositive)
            tvPositive.setOnClickListener {
                dlgWarning.dismiss()
                bindView()
            }
            val tvNegative = dlgWarning.findViewById<TextView>(R.id.tvNegative)
            tvNegative.setOnClickListener {
                dlgWarning.dismiss()
            }
            dlgWarning.show()
        }
    }

    private fun bindView(){
        lstMatchingModel?.clear()
        firstMatchingModel = MatchingModel()
        secondMatchingModel = MatchingModel()
        countMatchingCard = 0
        clearSelectItem()

        val matching = getRandomContentMatchingModel()
        var count = 0
        while ( lstMatchingModel.size < 7 ){
            val matchingModel = MatchingModel("","",false)
            matchingModel.img = matching.contentPostureImg[count].img
            matchingModel.postureName = matching.contentPostureImg[count].postureName
            matchingModel.isRenderImg = true
            lstMatchingModel.add( matchingModel )

            count++
        }

        count = 0
        while ( lstMatchingModel.size < 14 ){
            val matchingModel = MatchingModel("","",false)
            matchingModel.img = matching.contentPostureName[count].img
            matchingModel.postureName = matching.contentPostureName[count].postureName
            matchingModel.isRenderImg = false
            lstMatchingModel.add( matchingModel )

            count++
        }

        matchingCardMax = lstMatchingModel.size
        lstMatchingModel.shuffle()

        gameMatchingAdapter = GameMatchingAdapter(this, lstMatchingModel , object : GameMatchingAdapter.ClickListener {
            override fun onClickItem(matchingGameModel: MatchingModel) {
                if( firstMatchingModel.isEmpty() ){
                    firstMatchingModel = matchingGameModel
                }else{
                    if( matchingGameModel.isRenderImg == firstMatchingModel.isRenderImg &&
                        matchingGameModel.postureName == firstMatchingModel.postureName &&
                        matchingGameModel.img == firstMatchingModel.img ){
                        Toast.makeText(this@GameMatchingActivity , "ไม่สามารถเลือกซ้ำได้" , Toast.LENGTH_SHORT).show()
                    }else{
                        //ป้องกัน กรณีที่ เลือกรูปเดียวกัน 2 ครั้ง
                        secondMatchingModel = matchingGameModel
                        if( !firstMatchingModel.isEmpty() && !secondMatchingModel.isEmpty() )
                            checkMatching()

                    }
                }
            }
        })

        recyclerView.layoutManager = GridLayoutManager( this , 3 )
//        gameMatchingAdapter!!.setHasStableIds(true)
        recyclerView.adapter = gameMatchingAdapter


        bindRemainMatching()
    }

    private fun bindRemainMatching(){
        var remainMatchingCard = 0
        remainMatchingCard = if( countMatchingCard <= 0 ){
            (matchingCardMax/2)
        }else{
            (matchingCardMax/2) - countMatchingCard
        }

        var wording = "เหลืออีก xx คู่"
        wording = wording.replace("xx", remainMatchingCard.toString())
        tvRemainCardMatching.text = wording
        scope.launch {
            tvRemainCardMatching.startAnimation(AnimationUtils.loadAnimation(this@GameMatchingActivity, R.anim.animation_fade))
            delay(1000)
            tvRemainCardMatching.clearAnimation()
        }
    }

    private fun checkMatching(){
        scope.launch {
            if( firstMatchingModel.img == secondMatchingModel.img &&
                firstMatchingModel.postureName == secondMatchingModel.postureName){
                countMatchingCard++
                val dialogMatch = showMatch().apply {
                    show()

                    window?.setLayout(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    window?.setBackgroundDrawableResource(android.R.color.transparent);
                }
                delay(700)
                remove()
                bindRemainMatching()
                dialogMatch.dismiss()
                if( lstMatchingModel.size<=0 ){
                    val intent = Intent( this@GameMatchingActivity , SummaryScoreQuestionActivity::class.java )
                    intent.putExtra("score" , countMatchingCard)
                    intent.putExtra("total" , (matchingCardMax/2))
                    intent.putExtra("game_matching" , "เกมจับคู่")
                    intent.putExtra("count_not_matching" , countWrong)
                    startActivity(intent)
                    finish()
                }else{
                    firstMatchingModel = MatchingModel()
                    secondMatchingModel = MatchingModel()

                    gameMatchingAdapter?.notifyDataSetChanged()
                }
            }else{
                countWrong++
                val dialog = showMissMatch().apply {
                    show()

                    window?.setLayout(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    window?.setBackgroundDrawableResource(android.R.color.transparent);
                }
                delay(700)
                dialog.dismiss()

                clearSelectItem()
                firstMatchingModel = MatchingModel()
                secondMatchingModel = MatchingModel()

                gameMatchingAdapter?.notifyDataSetChanged()
            }
        }
    }

    private fun getIndexSelected():Int{
        for( i in 0..lstMatchingModel.size-1 ){
            if( lstMatchingModel[i].isSelect ){
                return i
            }
        }

        return -1
    }

    private fun remove(){
        do {
            val index = getIndexSelected()
            if( index > -1 )
                lstMatchingModel.removeAt(index)

        } while (index>-1)

        firstMatchingModel = MatchingModel()
        secondMatchingModel = MatchingModel()
    }

    private fun clearSelectItem(){
        for( i in 0..lstMatchingModel.size-1 ){
            lstMatchingModel[i].isSelect = false
        }
    }

    override fun onBackPressed() {
//        super.onBackPressed()

        llRootNavigator.performClick()
    }
}