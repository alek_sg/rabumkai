package com.rabumkai.thaidanceproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.rabumkai.thaidanceproject.model.VocabModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_detail_vocab.*

class DetailVocabActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_vocab)

        val bundle = intent.extras
        if( bundle == null ){
            Toast.makeText(this , "Something wrong" , Toast.LENGTH_SHORT).show()
            finish()
        }else{
            val vocab = bundle.getString("vocab")
            val vocabModel = Gson().fromJson(vocab , VocabModel::class.java)
            bindView(vocabModel)
        }
    }

    private fun bindView( vocabModel: VocabModel )
    {
        tvMean.text = vocabModel.mean
        tvVocabulary.text = vocabModel.vocabulary
        tvPronounce.text = vocabModel.pronounce
    }
}