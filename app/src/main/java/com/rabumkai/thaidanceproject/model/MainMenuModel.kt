package com.rabumkai.thaidanceproject.model

import com.google.firebase.database.DataSnapshot
import com.google.gson.annotations.SerializedName

data class MainMenuModel(var id:String = "",
                         var menu:String = "",
                         var url:String = "",
                         var key: String = "") {
}