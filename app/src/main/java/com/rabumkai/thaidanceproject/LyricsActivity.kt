package com.rabumkai.thaidanceproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.text.color
import com.adapter.office.Extention.showVocab
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rabumkai.thaidanceproject.model.Song
import com.rabumkai.thaidanceproject.model.SongModel
import com.rabumkai.thaidanceproject.model.VocabModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.activity_lyrics.*
import kotlinx.android.synthetic.main.activity_lyrics.ivBlur
import kotlinx.android.synthetic.main.activity_lyrics.llRoot
import kotlinx.android.synthetic.main.layout_action_bar.*
import java.util.ArrayList

class LyricsActivity : AppCompatActivity() {
    private var lstVocabModel:ArrayList<VocabModel> = arrayListOf()
    private var lstSongModel:ArrayList<SongModel> = arrayListOf()
    private var song:Song? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lyrics)

        val bundle = intent.extras
        val vocabData = bundle?.getString("vocab")!!
        val data = bundle?.getString("data")!!

        tvTitleNavigator.text = "เนื้อร้อง"
        layoutActionBar.setBackgroundColor(resources.getColor(R.color.color_title_music))

        val vocabType = object : TypeToken<ArrayList<VocabModel?>?>() {}.type
        lstVocabModel = Gson().fromJson(vocabData, vocabType)

        song = Gson().fromJson(data, Song::class.java)
        val lstLyrics = song!!.content.chunked(2)
        bindView(lstLyrics)

        Glide.with(this)
            .load(R.drawable.bg)
            .apply(
                RequestOptions.bitmapTransform(
                    BlurTransformation(25, 3)
                )
            ).into(ivBlur)
    }

    private fun bindView( lstLyrics:List<List<String>> ){
        for (countGroupRow in lstLyrics.indices) {

            //create row
            val vRow = layoutInflater.inflate(R.layout.row_lyrics , null)
            vRow.apply {
                val tvLyrics1 = findViewById<TextView>(R.id.tvLyrics1) as TextView
//                val tvLyrics2 = findViewById<TextView>(R.id.tvLyrics2) as TextView

                val dataRow0 = lstLyrics[countGroupRow][0]
//                val dataRow1 = lstLyrics[countGroupRow][1]

                if( dataRow0.contains("#" , true) || dataRow0.contains("*" , true) ){
                    val startIndex = dataRow0.indexOf("#" , 0)
                    val endIndex = dataRow0.indexOf("*" , 0)
                    val result1 = dataRow0.substring(startIndex+1,endIndex)
                    val spannableString = SpannableStringBuilder()
                    val action1ClickedSpan = object : ClickableSpan() {
                        override fun onClick(widget: View) {
                            val word = widget as TextView
                            openDialog(word.tag.toString())
                        }
                    }
                    spannableString.color(resources.getColor(R.color.blue_dark)){
                        append( dataRow0.subSequence(0,startIndex) )
                    }.color(resources.getColor(R.color.colorAccent)){
                        append( result1 )
                    }.color(resources.getColor(R.color.colorAccent)){
                        append( dataRow0.subSequence(endIndex,dataRow0.length-1) )
                    }.setSpan( action1ClickedSpan , startIndex , endIndex-1 , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    tvLyrics1.text = spannableString
                    tvLyrics1.tag = result1
                }else{
                    tvLyrics1.text = dataRow0
                }

                tvLyrics1.movementMethod = LinkMovementMethod.getInstance()

//                if( dataRow1.contains("#" , true) || dataRow1.contains("*" , true) ){
//                    val startIndex = dataRow1.indexOf("#" , 0)
//                    val endIndex = dataRow1.indexOf("*" , 0)
//                    val result1 = dataRow1.substring(startIndex+1,endIndex)
//                    val spannableString = SpannableStringBuilder()
//
//                    val action1ClickedSpan = object : ClickableSpan() {
//                        override fun onClick(widget: View) {
//                            val word = widget as TextView
//                            openDialog(word.tag.toString())
//                        }
//                    }
//
//                    spannableString.color(resources.getColor(R.color.blue_dark)){
//                        append( dataRow1.subSequence(0,startIndex) )
//                    }.color(resources.getColor(R.color.colorAccent)){
//                        append( result1 )
//                    }.color(resources.getColor(R.color.colorAccent)){
//                        append( dataRow1.subSequence(endIndex,dataRow1.length-1) )
//                    }.setSpan( action1ClickedSpan , startIndex , endIndex-1 , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//                    tvLyrics2.text = spannableString
//                    tvLyrics2.tag = result1
//                }else{
//                    tvLyrics2.text = dataRow1
//                }
//
//                tvLyrics2.movementMethod = LinkMovementMethod.getInstance()
            }

            val vRow1 = layoutInflater.inflate(R.layout.row_lyrics , null)
            vRow1.apply {
                val tvLyrics1 = findViewById<TextView>(R.id.tvLyrics1) as TextView
//                val tvLyrics2 = findViewById<TextView>(R.id.tvLyrics2) as TextView

                val dataRow0 = lstLyrics[countGroupRow][1]
//                val dataRow1 = lstLyrics[countGroupRow][3]

                if( dataRow0.contains("#" , true) || dataRow0.contains("*" , true) ){
                    val startIndex = dataRow0.indexOf("#" , 0)
                    val endIndex = dataRow0.indexOf("*" , 0)
                    val result1 = dataRow0.substring(startIndex+1,endIndex)
                    val spannableString = SpannableStringBuilder()

                    val action1ClickedSpan = object : ClickableSpan() {
                        override fun onClick(widget: View) {
                            val word = widget as TextView
                            openDialog(word.tag.toString())
                        }
                    }

                    spannableString.color(resources.getColor(R.color.blue_dark)){
                        append( dataRow0.subSequence(0,startIndex) )
                    }.color(resources.getColor(R.color.colorAccent)){
                        append( result1 )
                    }.color(resources.getColor(R.color.colorAccent)){
                        append( dataRow0.subSequence(endIndex,dataRow0.length-1) )
                    }.setSpan( action1ClickedSpan , startIndex , endIndex-1 , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

                    tvLyrics1.text = spannableString
                    tvLyrics1.tag = result1
                }else{
                    tvLyrics1.text = dataRow0
                }
                tvLyrics1.movementMethod = LinkMovementMethod.getInstance()

//                if( dataRow1.contains("#" , true) || dataRow1.contains("*" , true) ){
//                    val startIndex = dataRow1.indexOf("#" , 0)
//                    val endIndex = dataRow1.indexOf("*" , 0)
//                    val result1 = dataRow1.substring(startIndex+1,endIndex)
//                    val spannableString = SpannableStringBuilder()
//                    val action1ClickedSpan = object : ClickableSpan() {
//                        override fun onClick(widget: View) {
//                            val word = widget as TextView
//                            openDialog(word.tag.toString())
//                        }
//                    }
//
//                    spannableString.color(resources.getColor(R.color.blue_dark)){
//                        append( dataRow1.subSequence(0,startIndex) )
//                    }.color(resources.getColor(R.color.colorAccent)){
//                        append( result1 )
//                    }.color(resources.getColor(R.color.colorAccent)){
//                        append( dataRow1.subSequence(endIndex,dataRow1.length-1) )
//                    }.setSpan( action1ClickedSpan , startIndex , endIndex-1 , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
//                    tvLyrics2.text = spannableString
//                    tvLyrics2.tag = result1
//                }else{
//                    tvLyrics2.text = dataRow1
//                }
//                tvLyrics2.movementMethod = LinkMovementMethod.getInstance()
            }

            //create group row
            val vGroupRow = layoutInflater.inflate(R.layout.layout_lyrics , null)
            vGroupRow.apply {
                val llGroupRoot = findViewById<LinearLayout>(R.id.llGroupRoot) as LinearLayout

                llGroupRoot.addView(vRow)
                llGroupRoot.addView(vRow1)
            }
            llRoot.addView(vGroupRow)
        }
    }

    private fun getVocab(word:String):VocabModel?{

        Log.e(this@LyricsActivity.packageName.toString(), "word = $word")

        for( i in 0..lstVocabModel.size-1 ){
            Log.e(this@LyricsActivity.packageName.toString(), "vocabulary = "+lstVocabModel[i].vocabulary)
            if( word == lstVocabModel[i].vocabulary ){
                return lstVocabModel[i]
            }
        }
        return null
    }

    private fun openDialog(vocab:String){
        getVocab( vocab ).apply {
            if( this == null ) return

            showVocab().apply {

                Log.e("openDialog","mean-->"+mean)

                findViewById<TextView>(R.id.tvMean).text = mean
                findViewById<TextView>(R.id.tvPronounce).text = pronounce
                findViewById<TextView>(R.id.tvVocabulary).text = vocabulary
                findViewById<CardView>(R.id.cardView).setOnClickListener {
                    this.dismiss()
                }

                show()

                window?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                window?.setBackgroundDrawableResource(android.R.color.transparent);
            }
        }
    }
}