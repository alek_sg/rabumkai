package com.rabumkai.thaidanceproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rabumkai.thaidanceproject.model.PostureX
import com.rabumkai.thaidanceproject.model.Song
import com.rabumkai.thaidanceproject.model.SongModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.android.synthetic.main.activity_menu_song.*
import kotlinx.android.synthetic.main.activity_menu_song.ivBlur
import kotlinx.android.synthetic.main.activity_menu_song.layoutActionBar
import kotlinx.android.synthetic.main.layout_action_bar.*
import kotlinx.android.synthetic.main.layout_action_bar.llRoot
import java.util.ArrayList

class MenuSongActivity : AppCompatActivity() {

    private var vocabData = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = intent.extras
        val titlePage = bundle?.getString("title")
        title = titlePage

        setContentView(R.layout.activity_menu_song)

//        tvTitle.text = titlePage
        layoutActionBar.setBackgroundColor(resources.getColor(R.color.color_title_music))


        val data = bundle?.getString("data")
        vocabData = bundle?.getString("vocab")!!

        val userListType = object : TypeToken<ArrayList<Song?>?>() {}.type
        val lstPostureX: ArrayList<Song> = Gson().fromJson(data, userListType)

        Glide.with(this)
            .load(R.drawable.bg)
            .apply(
                RequestOptions.bitmapTransform(
                    BlurTransformation(25, 3)
                )
            ).into(ivBlur)

        bindView(lstPostureX)
    }

    private fun bindView(lstPostureX:ArrayList<Song> ){
//        llRoot.removeAllViews()

        for( i in 0..lstPostureX.size-1 ){
            val view = layoutInflater.inflate(R.layout.layout_sub_menu,null)
            val cvRootSubMenu = view.findViewById<CardView>(R.id.cvRootSubMenu)
            val tvSubMenu = view.findViewById<TextView>(R.id.tvSubMenu)

            cvRootSubMenu.setBackgroundColor(resources.getColor(R.color.color_title_music))

            tvSubMenu.text = lstPostureX[i].sub_menu
            view.tag = lstPostureX[i].id
            view.setOnClickListener {
                if( it.tag == "1" ){
                    val data = Gson().toJson(lstPostureX[i])
                    val intent = Intent( this@MenuSongActivity , LyricsActivity::class.java )
                    intent.putExtra("data" , data)
                    intent.putExtra("vocab" , vocabData)
                    intent.putExtra("title" , lstPostureX[i].sub_menu)
                    startActivity(intent)
                }else{
                    var resource = 0
                    resource = if( it.tag == "2" )
                        R.raw.merody_compress
                    else
                        R.raw.karaoke_compress

                    val intent = Intent( this@MenuSongActivity , ActingActivity::class.java )
                    intent.putExtra("resource" , resource)
                    intent.putExtra("title" , lstPostureX[i].sub_menu)
                    startActivity(intent)
                }
            }

            llRoot.addView(view)
        }
    }
}