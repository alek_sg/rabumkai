package com.rabumkai.thaidanceproject

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.util.TypedValue
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.adapter.office.Extention.showCorrectQuestion
import com.adapter.office.Extention.showFailedQuestion
import com.rabumkai.thaidanceproject.model.FillWordModel
import com.rabumkai.thaidanceproject.model.QuestionFillWordModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_fill_word.*
import kotlin.collections.HashMap
import kotlin.collections.set

class FillWordActivity : BaseActivity() {
    private var questionFillWordModel:QuestionFillWordModel? = null
    private var currentCount = 0
    private var itemsAnswer = HashMap<String, String>()
    private var event_form_after_song_activity = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fill_word)
        val bundle = intent.extras
        val data = bundle?.getString("data")!!
        val title = bundle?.getString("title")!!

        if( bundle.containsKey("event") )
            event_form_after_song_activity = bundle?.getString("event")!!

        tvTitleNavigator.text = title

        questionFillWordModel = Gson().fromJson(data, QuestionFillWordModel::class.java)
        tvCountQuestion.text = "ข้อที่ 1/"+getAmountQuestion()
        bindView( questionFillWordModel!!.content[currentCount] )
        bindEvent()

//        edtAnswer.setText("")
    }

    private fun generateHint( hint:String , answerRaw: String ){
        try{
            var isSpecial = false
            var canAddView = true
            val answer = answerRaw.replace("," , "")
            var countAnswer = 0
            llContainerHint.removeAllViews()
            for( i in 0..hint.length-1 ){
                if( hint[i] == '_' ){
                    val edt = EditText(this@FillWordActivity)
                    edt.filters =  arrayOf(InputFilter.LengthFilter(1))
                    edt.setSingleLine()
                    edt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14.toFloat())
                    edt.tag = answer[countAnswer]
                    countAnswer++
                    llContainerHint.addView(edt)
                }else{
                    val tv = TextView(this@FillWordActivity)
                    tv.setTextSize(TypedValue.COMPLEX_UNIT_SP,14.toFloat())
                    if( currentCount == 0 ){
                        if( i >= hint.length-2 ){
                            if( !isSpecial ){
                                tv.text = hint[i].toString()+hint[i+1].toString()
                                isSpecial = true
                                llContainerHint.addView(tv)
                            }
                        }else{
                            tv.text = hint[i].toString()
                            llContainerHint.addView(tv)
                        }

                    }else{
                        tv.text = hint[i].toString()
                        llContainerHint.addView(tv)
                    }
                }
            }
        }catch (ex:Exception){
            Log.e("","ex = "+ex.toString())
        }
    }

    private fun isLastPage() = (currentCount == questionFillWordModel!!.content!!.size-1)
    private fun isFirstPage() = (currentCount == 0)

    private fun bindView( fillWorkModel:FillWordModel  ){

        llContainerFillWord.removeAllViews()
        val vRow = layoutInflater.inflate(R.layout.row_fill_word , null)
        vRow.apply {
            val llContainerLyrics1 = findViewById<TextView>(R.id.llContainerLyrics1) as LinearLayout

            val tv = TextView(this@FillWordActivity)
            tv.text = fillWorkModel.question
            tv.setTextColor(resources.getColor(android.R.color.black))
            tv.setTextSize(TypedValue.COMPLEX_UNIT_SP,14.toFloat())

            llContainerLyrics1.tag = ""
            llContainerLyrics1.addView(tv)
        }
        llContainerFillWord.addView(vRow)

        generateHint( fillWorkModel.hint , fillWorkModel.answer )
    }

    private fun checkAnswer(question:String , answer:String):Boolean{

        for( count in 0..questionFillWordModel!!.content.size-1 ){
            val answerModel = questionFillWordModel!!.content[count].answer
            var questionModel = questionFillWordModel!!.content[count].question
            questionModel = questionModel.replace("*" , "")

            if( (answer == answerModel) ){
                if( (questionModel == question) ){
                    return true
                }
            }
        }

        return false
    }

    private fun getAmountQuestion():Int{
//        return 2
        return questionFillWordModel!!.content.size
    }

    private fun getScore():Int{

        var score = 0
        for( i in 0 until questionFillWordModel!!.content.size ){
            questionFillWordModel!!.content[i].apply {
                var answerClaerWhilteSpace = itemsAnswer[question]
                var displayAnswer = answerClaerWhilteSpace?.replace("\\s".toRegex(), "")
                if( final_answer == displayAnswer){
                    score++
                }
            }
        }

        return score
    }

    private fun nextQuestion(){
        currentCount++
        if( currentCount < getAmountQuestion() ){

            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(
                edtAnswer,
                InputMethodManager.SHOW_IMPLICIT)

            bindView(questionFillWordModel!!.content[currentCount])

            val tmp = currentCount+1
            tvCountQuestion.text = "ข้อที่ "+tmp+"/"+getAmountQuestion()
        }else{
            val dataFillWord = Gson().toJson(questionFillWordModel!!.content)

            val intent = Intent( this , SummaryScoreQuestionActivity::class.java )
            intent.putExtra("answer",itemsAnswer)
            intent.putExtra("list_fill_word",dataFillWord)
            intent.putExtra("fill_word",true)
            intent.putExtra("total",itemsAnswer.size)
            intent.putExtra("score",getScore())

            if( event_form_after_song_activity.isNotEmpty() )
                intent.putExtra("chapter" , event_form_after_song_activity)

            startActivity(intent)

            finish()
        }
    }

    private fun bindEvent(){

        llRootNavigator.setOnClickListener {
            finish()
        }

        btnCheckAnswer.setOnClickListener {
            val myAnswer = getMyAnswer()
            val answer = getAnswer()
            val question = questionFillWordModel!!.content[currentCount].question
            val isCorrect = checkAnswer( question.replace("*" , "") , answer)
//            val isCorrect = checkAnswer( question.replace("*" , "") , edtAnswer.text.toString())

            itemsAnswer[question] = myAnswer
//            itemsAnswer[question] = edtAnswer.text.toString()

            if( isCorrect){
                var displayAnswer = myAnswer?.replace("\\s".toRegex(), "")
                val dlgCorrectQuestion = this.showCorrectQuestion(displayAnswer,question)
                dlgCorrectQuestion.window?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                dlgCorrectQuestion.window?.setBackgroundDrawableResource(android.R.color.transparent);

                val btnNext = dlgCorrectQuestion.findViewById<Button>(R.id.btnNext)
                if( isLastPage() ){
                    btnNext.text = "สรุปคะแนน"
                }

                btnNext.setOnClickListener {
                    edtAnswer.setText("")
                    nextQuestion()
                    dlgCorrectQuestion.dismiss()
                }
                dlgCorrectQuestion.show()
            }else{

                val finalAnswer = questionFillWordModel!!.content[currentCount].final_answer
//                val myAnswer = itemsAnswer[questionFillWordModel!!.content[currentCount].question]!!

                val dlgFailedQuestion = this.showFailedQuestion(question.replace("*" , "___"), finalAnswer, myAnswer , false)
                dlgFailedQuestion.window?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                dlgFailedQuestion.window?.setBackgroundDrawableResource(android.R.color.transparent);

                val btnNext = dlgFailedQuestion.findViewById<Button>(R.id.btnNext)
                if( isLastPage() ){
                    btnNext.text = "สรุปคะแนน"
                }

                btnNext.setOnClickListener {
                    edtAnswer.setText("")
                    nextQuestion()
                    dlgFailedQuestion.dismiss()

                }

                dlgFailedQuestion.show()
            }
        }

        llRootNavigator.setOnClickListener {
            finish()
        }
    }

    private fun getAnswer():String{
        var answer = ""
        for( i in 0..llContainerHint.childCount-1 ){

            if( llContainerHint.getChildAt(i) is EditText ){
                if( llContainerHint.getChildAt(i).tag != null ){
                    answer+=(llContainerHint.getChildAt(i) as EditText).text.toString()+","
                }
            }
        }
        //remove ","
        answer = answer.substring(0,answer.length-1)
        return answer
    }

    private fun getMyAnswer():String{
        var myAnswer = ""
        for( i in 0..llContainerHint.childCount-1 ){
            if( llContainerHint.getChildAt(i) is EditText ){
                myAnswer+=(llContainerHint.getChildAt(i) as EditText).text.toString()
            }else if( llContainerHint.getChildAt(i) is TextView ){
                val tv = (llContainerHint.getChildAt(i) as TextView)
                if(tv.text.toString() != "")
                    myAnswer+= tv.text.toString()
            }
        }

        return myAnswer
    }
}