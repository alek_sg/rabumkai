package com.rabumkai.thaidanceproject

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.rabumkai.thaidanceproject.adapter.SectionsPagerPostureAdapter
import com.rabumkai.thaidanceproject.model.Content
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_parent.*
import kotlinx.android.synthetic.main.layout_action_bar.*
import kotlinx.android.synthetic.main.layout_action_bar.llRootNavigator
import java.util.ArrayList

class ParentPostureActivity : BaseActivity() {

    var lstContent = ArrayList<Content>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var data = ""
        var titlePage = ""

        val bundle = intent.extras
        if( bundle != null ){
            data = bundle.getString("data").toString()

            titlePage = bundle.getString("title")!!
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        }

        setContentView(R.layout.activity_parent)
        val sectionsPagerAdapter = SectionsPagerPostureAdapter(
                this,
                supportFragmentManager)

        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter

        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)

        if( bundle!!.containsKey("data_body") ){
            val data = bundle.getString("data_body")
            val lstType = object : TypeToken<ArrayList<Content?>?>() {}.type
            lstContent = Gson().fromJson(data, lstType)
        }

        llRootNavigator.setOnClickListener { finish() }

        playVdoIntroduce(this , R.raw.introduce_posture)

        llNextChapter.setOnClickListener {
            gotoSongActivity()
            finish()
        }


    }

    private fun gotoSongActivity(){
        val dataSong = getSong()
        val dataVocab = getDataVocab()
        val data = Gson().toJson(dataSong)
        val vocabData = Gson().toJson(dataVocab)
        val title = "คำร้องและทำนอง"

        val intent = Intent( this , ParentSongActivity::class.java )
        intent.putExtra("data" , data )
        intent.putExtra("vocab" , vocabData )
        intent.putExtra("title" , title )
        startActivity(intent)
    }
}