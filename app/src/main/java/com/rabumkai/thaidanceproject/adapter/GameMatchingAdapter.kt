package com.rabumkai.thaidanceproject.adapter;

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rabumkai.thaidanceproject.BaseActivity
import com.rabumkai.thaidanceproject.R
import com.rabumkai.thaidanceproject.model.MatchingModel
import kotlinx.android.synthetic.main.layout_matching_game.view.*


class GameMatchingAdapter(val context: Context, val lstPostureActionModel: ArrayList<MatchingModel>, val listener:ClickListener) : RecyclerView.Adapter<GameMatchingAdapter.ViewHolder>() {

    interface ClickListener{
        fun onClickItem(model: MatchingModel)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(lstPostureActionModel[position] )
        holder.itemView.setOnClickListener {

            lstPostureActionModel[position].isSelect = true
            listener.onClickItem(lstPostureActionModel[position])
            holder.bind(lstPostureActionModel[position] )
        }
    }

//    override fun getItemId(position: Int): Long {
//        val matchingModel= lstPostureActionModel[position]
//        return matchingModel.id.toLong()
//    }

    override fun getItemCount() = lstPostureActionModel.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return  ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_matching_game , parent,false))
    }

    class ViewHolder(itemsView: View ): RecyclerView.ViewHolder(itemsView) {

        fun bind(model: MatchingModel ) {
            itemView.apply {
                try {
                    if( model.isRenderImg ){
                        val baseActivity = context as BaseActivity
                        val resource = baseActivity.getResourceId(context , model.img, "drawable" , baseActivity.packageName)
                        Glide.with(baseActivity).load(resource).into(ivPosture)

                        //Cheating answer
                        tvFakePosture.text = model.postureName
                        tvFakePosture.visibility = View.GONE

                        ivPosture.visibility = View.VISIBLE
                        cvPostureImage.visibility = View.VISIBLE
                        cvPostureName.visibility = View.GONE
                    }else{
                        tvPosture.text = model.postureName

                        cvPostureImage.visibility = View.GONE
                        cvPostureName.visibility = View.VISIBLE
                    }

                    if( model.isSelect ){
                        if( model.isRenderImg )
                            cvPostureImage.startAnimation(AnimationUtils.loadAnimation(context, R.anim.bounce_logo))
                        else
                            cvPostureName.startAnimation(AnimationUtils.loadAnimation(context, R.anim.bounce_logo))

                        ivSelect.visibility = View.GONE
                    }else{
                        if( model.isRenderImg )
                            cvPostureImage.clearAnimation()
                        else
                            cvPostureName.clearAnimation()

                        ivSelect.visibility = View.GONE
                    }
                } catch ( ex:Exception) {
                    Log.e("LitsRedeemAdapter" , "ex = "+ex.toString())
                }
            }
        }
    }
}