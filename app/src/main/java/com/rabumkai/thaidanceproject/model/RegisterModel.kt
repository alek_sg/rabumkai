package com.rabumkai.thaidanceproject.model

import com.google.firebase.database.PropertyName

data class RegisterModel(@PropertyName("username") var username:String = "",
                         @PropertyName("password") var password:String = "",
                         @PropertyName("classroom")var classroom:String = "",
                         @PropertyName("name") var name:String = "",
                         @PropertyName("lastname") var lastname:String = "",
                         @PropertyName("after_study") var after_study:String = "",
                         @PropertyName("before_study") var before_study:String = "",
                         @PropertyName("correct_or_wrong") var correct_or_wrong:String = "",
                         @PropertyName("fill_word") var fill_word:String = "",
                         @PropertyName("key") var key:String = "",
                         @PropertyName("is_done_exercise") var is_done_exercise:String = "",
                         @PropertyName("is_done_exercise_fill_word") var is_done_exercise_fill_word:String = "") {

    override fun toString():String{
        return "user name-> "+username+
                " \npassword-> "+password+
                " \nclass room-> "+classroom+
                " \nname-> "+name+
                " \nlast name-> "+lastname+
                " \nafter_study-> "+after_study+
                " \nbefore_study-> "+before_study+
                " \ncorrect_or_wrong-> "+correct_or_wrong+
                " \nfill_word-> "+fill_word+
                " \nkey-> "+key+
                " \nis_done_exercise-> "+is_done_exercise+
                " \nis_done_exercise_fill_word-> "+is_done_exercise_fill_word

    }
}