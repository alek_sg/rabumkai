package com.rabumkai.thaidanceproject.fragment

import adapter.com.extention.Triangle
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.rabumkai.thaidanceproject.LoginActivity
import com.rabumkai.thaidanceproject.R
import com.orhanobut.hawk.Hawk
import com.rabumkai.thaidanceproject.BaseApplication
import kotlinx.android.synthetic.main.fragment_about.*

/**
 * A placeholder fragment containing a simple view.
 */

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class AboutFragment: Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AboutFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindEvent()
    }

    private fun bindEvent(){
        flContainerAbout.setOnClickListener {
            if( flContainerAbout.tag.toString() == "show" ){
                ivCloseThakyou.rotation = 0f
                llThankYou.visibility = View.GONE
                flContainerAbout.tag = "hide"
            }else{
                ivCloseThakyou.rotation = 180f
                llThankYou.visibility = View.VISIBLE
                flContainerAbout.tag = "show"
            }
        }

        flContainerExit.setOnClickListener {
            Hawk.deleteAll()

            (requireActivity().application as BaseApplication).isDoneExerciseBeforeStudy = false

            val intent = Intent( activity , LoginActivity::class.java )
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            activity!!.finish()
        }

    }
}