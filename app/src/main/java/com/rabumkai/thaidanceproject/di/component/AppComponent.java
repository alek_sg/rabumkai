package com.rabumkai.thaidanceproject.di.component;


import com.rabumkai.thaidanceproject.di.modules.AppModule;
import com.rabumkai.thaidanceproject.di.modules.DataSourceModule;
import com.rabumkai.thaidanceproject.features.FullscreenActivity;
import com.rabumkai.thaidanceproject.features.gamehistory.GameHistoryActivity;
import com.rabumkai.thaidanceproject.features.gameover.GameOverActivity;
import com.rabumkai.thaidanceproject.features.gameplay.GamePlayActivity;
import com.rabumkai.thaidanceproject.features.mainmenu.MainMenuActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by abdularis on 18/07/17.
 */

@Singleton
@Component(modules = {AppModule.class, DataSourceModule.class})
public interface AppComponent {

    void inject(GamePlayActivity activity);

    void inject(MainMenuActivity activity);

    void inject(GameOverActivity activity);

    void inject(FullscreenActivity activity);

    void inject(GameHistoryActivity activity);

}
