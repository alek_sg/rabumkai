package com.rabumkai.thaidanceproject.fragment

import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.MediaController
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.text.color
import androidx.fragment.app.Fragment
import com.adapter.office.Extention.showVocab
import com.bumptech.glide.Glide
import com.rabumkai.thaidanceproject.FullScreenVideoActivity
import com.rabumkai.thaidanceproject.R
import com.rabumkai.thaidanceproject.model.Song
import com.rabumkai.thaidanceproject.model.SongModel
import com.rabumkai.thaidanceproject.model.VocabModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_tab1_song.*
import kotlinx.android.synthetic.main.layout_action_bar.*
import java.util.*

/**
 * A placeholder fragment containing a simple view.
 */
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val ARG_SEEK_TO = "seek_to"

class Tab1SongFragment : Fragment() {
    private var lstVocabModel:ArrayList<VocabModel> = arrayListOf()
    private var lstSongModel:ArrayList<SongModel> = arrayListOf()
    private var song: Song? = null

    private var param1: String? = null
    private var param2: String? = null
    private var seekTo = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
            seekTo = it.getInt(ARG_SEEK_TO)
        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab1_song, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String , seekTo: Int) =
            Tab1SongFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                    putInt(ARG_SEEK_TO, seekTo)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val vocabData = param1
        val data = param2

        val vocabType = object : TypeToken<ArrayList<VocabModel?>?>() {}.type
        lstVocabModel = Gson().fromJson(vocabData, vocabType)

        val userListType = object : TypeToken<ArrayList<Song?>?>() {}.type
        val lstPostureX: ArrayList<Song> = Gson().fromJson(data, userListType)

        song = lstPostureX[0]
        val lstLyrics = song!!.content.chunked(2)
        bindView(lstLyrics)

        bindVDO()

        ivFullScreen.setOnClickListener {
            val intent = Intent( activity , FullScreenVideoActivity::class.java )
            intent.putExtra("seek" , vdoViewTab1!!.currentPosition)
            intent.putExtra("resource" ,R.raw.merody_compress )
            intent.putExtra("requestCode" ,2001 )
            startActivityForResult(intent , 2001)
        }

        ivPlayVdoTab1.visibility = View.VISIBLE
        ivPlayVdoTab1.setOnClickListener {
            vdoViewTab1!!.seekTo( seekTo )
            vdoViewTab1!!.start()
            ivPlayVdoTab1.visibility = View.GONE
        }

        Log.e("Tab1SongFragment" , "onViewCreated")
    }

    private fun bindVDO(){
        val mediaController = MediaController(activity)
        mediaController.setAnchorView(vdoViewTab1)
        vdoViewTab1!!.setMediaController(mediaController)
        playVdo(R.raw.merody_compress , seekTo)
    }

    private fun playVdo(idResource:Int , seekTo:Int){
        try{
            val path = "android.resource://com.rabumkai.thaidanceproject/$idResource"
            vdoViewTab1!!.setVideoURI(Uri.parse(path))
            vdoViewTab1!!.setOnPreparedListener(MediaPlayer.OnPreparedListener { mp ->
                mp.isLooping = false
                mp.setScreenOnWhilePlaying(false)
                vdoViewTab1!!.seekTo( seekTo )
            })
        }catch (e:Exception){
            Log.e("Tab1SongFragment", "playVdo-->$e")
        }
    }

    private fun bindView( lstLyrics:List<List<String>> ){
        for (countGroupRow in lstLyrics.indices) {

            //create row
            val vRow = layoutInflater.inflate(R.layout.row_lyrics , null)
            vRow.apply {
                val tvLyrics1 = findViewById<TextView>(R.id.tvLyrics1) as TextView
                val dataRow0 = lstLyrics[countGroupRow][0]

                if( dataRow0.contains("#" , true) || dataRow0.contains("*" , true) ){
                    val startIndex = dataRow0.indexOf("#" , 0)
                    val endIndex = dataRow0.indexOf("*" , 0)
                    val result1 = dataRow0.substring(startIndex+1,endIndex)
                    val spannableString = SpannableStringBuilder()
                    val action1ClickedSpan = object : ClickableSpan() {
                        override fun onClick(widget: View) {
                            val word = widget as TextView
                            openDialog(word.tag.toString())
                        }
                    }
                    spannableString.color(resources.getColor(android.R.color.black)){
                        append( dataRow0.subSequence(0,startIndex) )
                    }.color(resources.getColor(R.color.green)){
                        append( result1 )
                    }.color(resources.getColor(R.color.green)){
                        append( dataRow0.subSequence(endIndex,dataRow0.length-1) )
                    }.setSpan( action1ClickedSpan , startIndex , endIndex-1 , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    tvLyrics1.text = spannableString
                    tvLyrics1.tag = result1
                }else{
                    tvLyrics1.text = dataRow0
                }

                tvLyrics1.movementMethod = LinkMovementMethod.getInstance()
            }

            val vRow1 = layoutInflater.inflate(R.layout.row_lyrics , null)
            vRow1.apply {
                val tvLyrics1 = findViewById<TextView>(R.id.tvLyrics1) as TextView
                if( lstLyrics[countGroupRow].size > 1 ){
                    val dataRow0 = lstLyrics[countGroupRow][1]

                    if( dataRow0.contains("#" , true) || dataRow0.contains("*" , true) ){
                        val startIndex = dataRow0.indexOf("#" , 0)
                        val endIndex = dataRow0.indexOf("*" , 0)
                        val result1 = dataRow0.substring(startIndex+1,endIndex)
                        val spannableString = SpannableStringBuilder()

                        val action1ClickedSpan = object : ClickableSpan() {
                            override fun onClick(widget: View) {
                                val word = widget as TextView
                                openDialog(word.tag.toString())
                            }
                        }

                        spannableString.color(resources.getColor(android.R.color.black)){
                            append( dataRow0.subSequence(0,startIndex) )
                        }.color(resources.getColor(R.color.green)){
                            append( result1 )
                        }.color(resources.getColor(R.color.green)){
                            append( dataRow0.subSequence(endIndex,dataRow0.length-1) )
                        }.setSpan( action1ClickedSpan , startIndex , endIndex-1 , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

                        tvLyrics1.text = spannableString
                        tvLyrics1.tag = result1
                    }else{
                        tvLyrics1.text = dataRow0
                    }
                    tvLyrics1.movementMethod = LinkMovementMethod.getInstance()
                }
            }

            //create group row
            val vGroupRow = layoutInflater.inflate(R.layout.layout_lyrics , null)
            vGroupRow.apply {
                val llGroupRoot = findViewById<LinearLayout>(R.id.llGroupRoot) as LinearLayout

                llGroupRoot.addView(vRow)
                llGroupRoot.addView(vRow1)
            }
            llRootLyric.addView(vGroupRow)
        }
    }

    private fun getVocab(word:String): VocabModel?{

        for( i in 0..lstVocabModel.size-1 ){
            if( word == lstVocabModel[i].vocabulary ){
                return lstVocabModel[i]
            }
        }
        return null
    }

    private fun openDialog(vocab:String){
        if( activity == null ) return

        getVocab( vocab ).apply {
            if( this == null ) return

            activity.showVocab().apply {
                findViewById<TextView>(R.id.tvMean).text = mean
                findViewById<TextView>(R.id.tvPronounce).text = pronounce
                findViewById<TextView>(R.id.tvVocabulary).text = vocabulary
                findViewById<ImageView>(R.id.ivClose).setOnClickListener {
                    this.dismiss()
                }
                findViewById<CardView>(R.id.cardView).setOnClickListener {
                    this.dismiss()
                }

                show()

                window?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                window?.setBackgroundDrawableResource(android.R.color.transparent);
            }
        }
    }

    override fun onPause() {
        super.onPause()

        if( vdoViewTab1 != null )
            vdoViewTab1!!.pause()
    }
    override fun onStop() {
        super.onStop()

        if( vdoViewTab1 != null )
            vdoViewTab1!!.pause()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if( data != null ){
            data.extras?.apply {
                if( vdoViewTab1 != null ){
                    playVdo( getInt("resource") , getInt("seek"))
                    vdoViewTab1!!.start()
                }else{
                    Log.e("Tab1SongFragment" , "vdoView == null")
                }
            }
        }else{
            Log.e("Tab1SongFragment" , "data == null")
        }
    }
}