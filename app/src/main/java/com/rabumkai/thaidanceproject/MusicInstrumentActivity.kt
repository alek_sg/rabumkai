package com.rabumkai.thaidanceproject

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.adapter.office.Extention.showLargeImage
import com.adapter.office.Extention.showPosture
import com.bumptech.glide.Glide
import com.rabumkai.thaidanceproject.adapter.MusicInstumentAdapter
import com.rabumkai.thaidanceproject.model.MusicalInstrumentModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_music_instrment.*
import kotlinx.android.synthetic.main.activity_music_instrment.tvTitle
import kotlinx.android.synthetic.main.activity_music_instrment.ivBannerHeader
import kotlinx.android.synthetic.main.activity_music_instrment.llRootNavigator
import kotlinx.android.synthetic.main.activity_music_instrment.toolbar
import kotlinx.android.synthetic.main.activity_music_instrment.tvTitleNavigator
import java.util.*


class MusicInstrumentActivity : BaseActivity() {

    var playAudio = MediaPlayer()
    var event = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        setContentView(R.layout.activity_music_instrment)
        val bundle = intent.extras
        if( bundle == null ) {
            Toast.makeText(this, "Something wrong", Toast.LENGTH_SHORT).show()
            finish()
            return
        }

        var titlePage = bundle.getString("title")


        toolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        tvTitle.text = titlePage
        tvTitleNavigator.text = titlePage
//        Glide.with(this)
//            .load(R.drawable.banner_header_music_equipment)
//            .into(ivBannerHeader)

        ivBannerHeader.setBackgroundColor(resources.getColor(R.color.colorPrimary))

        llMusic1.setOnClickListener {
            val model = getModel( tvMusicEquipment1.text.toString() , "a_111" )
            showDialog(model , R.raw.musical_instrument_1 )
        }
        llMusic2.setOnClickListener {
            val model = getModel( tvMusicEquipment2.text.toString() , "a_222" )
            showDialog(model , R.raw.musical_instrument_2 )
        }
        llMusic3.setOnClickListener {
            val model = getModel( tvMusicEquipment3.text.toString() , "a_10" )
            showDialog(model , R.raw.musical_instrument_3 )
        }
        llMusic4.setOnClickListener {
            val model = getModel( tvMusicEquipment4.text.toString() , "a_444" )
            showDialog(model , R.raw.musical_instrument_4 )
        }
        llMusic5.setOnClickListener {
            val model = getModel( tvMusicEquipment5.text.toString() , "a_1212" )
            showDialog(model , R.raw.musical_instrument_5 )
        }
        llMusic6.setOnClickListener {
            val model = getModel( tvMusicEquipment6.text.toString() , "a_555" )
            showDialog(model , R.raw.musical_instrument_6 )
        }
        llMusic7.setOnClickListener {
            val model = getModel( tvMusicEquipment7.text.toString() , "a_666" )
            showDialog(model , R.raw.musical_instrument_7 )
        }

        llMusic8.setOnClickListener {
            val model = getModel( tvMusicEquipment8.text.toString() , "a_9999" )
            showDialog(model , R.raw.musical_instrument_8 )
        }

        llMusic9.setOnClickListener {
            val model = getModel( tvMusicEquipment9.text.toString() , "a_777" )
            showDialog(model , R.raw.musical_instrument_9 )
        }

        llMusic10.setOnClickListener {
            val model = getModel( tvMusicEquipment10.text.toString() , "a_333" )
            showDialog(model , R.raw.musical_instrument_10 )
        }

        llRootNavigator.setOnClickListener {
            playAudio.stop()
            finish()
        }

        playVdoIntroduce(this , R.raw.introduce_instrument)

        tvVdoIntroduce.setOnClickListener {
            playVdoIntroduce(this , R.raw.introduce_instrument)
        }

        llNextChapter.setOnClickListener {

            val data = getQuiz()

            val questionModel = Gson().toJson(data[2])
            val intent = Intent( this , CorrectOrWrongActivity::class.java )
            intent.putExtra("data" , questionModel)
            startActivity(intent)

            finish()
        }
    }

    private fun getModel(key:String , nameDrawale:String):MusicalInstrumentModel{
        return MusicalInstrumentModel( "" , nameDrawale , key)
    }

    private fun showDialog(model: MusicalInstrumentModel , idResource:Int){

        playAudio.stop()
        playAudio = MediaPlayer.create(this , idResource)
        playAudio.start()

        val dlgLargeImage = showLargeImage().apply {
                    val ivPosture = findViewById<ImageView>(R.id.ivMenu)
                    val tvContent = findViewById<TextView>(R.id.tvContent)
                    val cardView = findViewById<CardView>(R.id.cardView)

                    tvContent.text = model.key
                    cardView.setOnClickListener {
                        playAudio.stop()
                        dismiss()
                    }

                    val resource = getResourceId(context , model.url , "drawable" , packageName)
                    Glide.with(this@MusicInstrumentActivity).load(resource).into(ivPosture)
                    show()

                    window?.setLayout(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    window?.setBackgroundDrawableResource(android.R.color.transparent);
                }

        dlgLargeImage.setOnDismissListener {
            playAudio.stop()
        }
    }

    override fun onStop() {
        super.onStop()

        playAudio.stop()
    }
}