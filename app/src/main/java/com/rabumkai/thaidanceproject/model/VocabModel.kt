package com.rabumkai.thaidanceproject.model

data class VocabModel(var mean:String = "",
                      var pronounce:String = "",
                      var vocabulary:String = "") {
}