package com.rabumkai.thaidanceproject

import android.content.Intent
import android.os.Bundle
import com.adapter.office.Extention.showDialogReadMore
import com.rabumkai.thaidanceproject.model.*
import com.google.firebase.database.*
import com.google.gson.Gson
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_main_menu_new.*
import org.json.JSONObject


class MainMenuActivityBk : BaseActivity() {
    var profileModel:ProfileModel ?= null

    var database: FirebaseDatabase? = null
    var dataSnapshot:DataSnapshot? = null

    val MENU_HISTORY = "1"
    val MENU_POSTURE = "2"
    val MENU_SONG = "3"
    val MENU_ACTING = "4"
    val MENU_COSTUME = "5"
    val MENU_SINGOUT = "6"
    val MENU_MUSIC_INSTRUMENT = "7"

    var jsonObject:JSONObject? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu_new)

//        if( !Hawk.isBuilt() ){
//            Hawk.init(this).build()
//        }
//
//        intent.extras?.apply {
//            if( containsKey("profile") ){
//                val data = Gson().fromJson(getString("profile") , ProfileModel::class.java)
//                profileModel = data
//            }
//        }

//        Glide.with(this)
//            .load(R.drawable.bg)
//            .apply(
//                RequestOptions.bitmapTransform(
//                    BlurTransformation(25, 3)
//                )
//            ).into(ivBackground)

//        getListVocabFromFirebase()
//        bindProfile()

        bindEvent()

        ivQuestion.setOnClickListener {
            val intent = Intent( this@MainMenuActivityBk , SubmenuQuestionActivity::class.java )
            val data = getQuiz()
            val dataQuestion = Gson().toJson(data)
            intent.putExtra("data" , dataQuestion)
            startActivity(intent)
        }

        ivGame.setOnClickListener {
            val intent = Intent(this@MainMenuActivityBk , MenuGameActivity::class.java)
            startActivity(intent)
        }
    }

    private fun bindEvent(){
        llHistory.setOnClickListener {
            startActivityByMenu(MENU_HISTORY , tvHistory.text.toString())
        }

        llRabum.setOnClickListener {
            startActivityByMenu(MENU_POSTURE , tvRabum.text.toString())
        }

        llSong.setOnClickListener {
            startActivityByMenu(MENU_SONG , tvMusic.text.toString())
        }

        llActing.setOnClickListener {
            startActivityByMenu(MENU_ACTING , tvActing.text.toString())
        }

        llCostume.setOnClickListener {
            startActivityByMenu(MENU_COSTUME , tvCostume.text.toString())
        }

        llMusicEquipment.setOnClickListener {
            startActivityByMenu(MENU_MUSIC_INSTRUMENT , tvMusicEquipment.text.toString())
        }
    }

    private fun startActivityByMenu( menuId:String , menuTitle:String ){
        when ( menuId ){
            MENU_HISTORY->{
                val intent = Intent( this@MainMenuActivityBk , HistoryActivity::class.java )
                val data = getHistory()
                intent.putExtra("data" , data )
                intent.putExtra("title" , menuTitle )
                startActivity(intent)
            }
            MENU_POSTURE->{
                val dataPosture = getPosture()
                val data = Gson().toJson(dataPosture)

//                val intent = Intent( this@MainMenuActivity , MenuPostureActivity::class.java )
                val intent = Intent( this@MainMenuActivityBk , ParentPostureActivity::class.java )
                intent.putExtra("data" , data )
                intent.putExtra("title" , menuTitle )
                startActivity(intent)
            }
            MENU_SONG->{
                val dataSong = getSong()
                val dataVocab = getDataVocab()
                val data = Gson().toJson(dataSong)
                val vocabData = Gson().toJson(dataVocab)
                val title = menuTitle

                val intent = Intent( this@MainMenuActivityBk , ParentSongActivity::class.java )
                intent.putExtra("data" , data )
                intent.putExtra("vocab" , vocabData )
                intent.putExtra("title" , title )
                startActivity(intent)
            }
            MENU_ACTING->{
                val intent = Intent( this@MainMenuActivityBk , ActingActivity::class.java )
                val title = menuTitle
                intent.putExtra("resource" , R.raw.acting )
                intent.putExtra("title" , title )
                startActivity(intent)
            }
            MENU_COSTUME->{
                val intent = Intent( this@MainMenuActivityBk , CostumeActivity::class.java )
                val data = getCostume()
                val title = menuTitle
                intent.putExtra("data" , data )
                intent.putExtra("title" , title )
                startActivity(intent)
            }
            MENU_MUSIC_INSTRUMENT->{
                val intent = Intent( this@MainMenuActivityBk , MusicInstrumentActivity::class.java )
                val lstMusicalInstrumentModel = getMusicalInstrumentModel()
                val title = menuTitle
                val data = Gson().toJson(lstMusicalInstrumentModel)

                intent.putExtra("data" , data )
                intent.putExtra("title" , title )
                startActivity(intent)
            }
            MENU_SINGOUT->{
                Hawk.deleteAll()

                val intent = Intent( this@MainMenuActivityBk , LoginActivity::class.java )
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                finish()
            }
        }
    }

    private fun bindProfile(){
//        profileModel?.apply {
//            if( name.isEmpty() ){
//                tvName.visibility = View.GONE
//                tvClassroom.visibility = View.GONE
//                cardView.visibility = View.GONE
//            }else{
//                tvName.text = "$name $lastname"
//                tvClassroom.text = classroom
//
//                tvName.visibility = View.VISIBLE
//                tvClassroom.visibility = View.VISIBLE
//
//                if( name.isEmpty() and lastname.isEmpty() )
//                    cardView.visibility = View.VISIBLE
//            }
//        }
    }

    override fun onDestroy() {
        super.onDestroy()

        Hawk.destroy()
    }
}