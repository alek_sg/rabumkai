package com.rabumkai.thaidanceproject

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.MediaController
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.gson.Gson
import com.rabumkai.thaidanceproject.fragment.Tab1SongFragment
import com.rabumkai.thaidanceproject.fragment.Tab2SongFragment
import kotlinx.android.synthetic.main.activity_parent_song.*
import kotlinx.android.synthetic.main.activity_parent_song.llRootNavigator
import kotlinx.android.synthetic.main.fragment_tab1_song.*
import kotlinx.android.synthetic.main.fragment_tab2_song.*

class ParentSongActivity : BaseActivity() {
    var data = ""
    var vocabData = ""
    var menuActive = 1
    var seekVdo1 = 12000
    var seekVdo2 = 12000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val bundle = intent.extras
        if( bundle != null ){

            vocabData = bundle?.getString("vocab")!!
            data = bundle?.getString("data")!!
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        }
        setContentView(R.layout.activity_parent_song)

        setActivityTab(1)
        initialFragment("tab1" , seekVdo1)

        llRootNavigator.setOnClickListener { finish() }
        flTabSong1.setOnClickListener {
            val fragment = getFragmentByTag("tab2")
            if( fragment != null ){
                if( (fragment as Tab2SongFragment).vdoViewTab2 != null ){
                    seekVdo2 = (fragment as Tab2SongFragment).vdoViewTab2.currentPosition
                }
            }

            setActivityTab(1)
            initialFragment("tab1" , seekVdo1)
        }

        flTabSong2.setOnClickListener {
            val fragment = getFragmentByTag("tab1")
            if( fragment != null ){
                if( (fragment as Tab1SongFragment).vdoViewTab2 != null ){
                    seekVdo1 = (fragment as Tab1SongFragment).vdoViewTab1.currentPosition
                }
            }

            setActivityTab(2)
            initialFragment("tab2" , seekVdo2)
        }

        llNextChapter.setOnClickListener {
            //ไป แบบฝึกหัดที่ 1
            val data = getQuiz()
            val questionModel = Gson().toJson(data[1])
            val intent = Intent( this , FillWordActivity::class.java )
            intent.putExtra("data" , questionModel)
            intent.putExtra("title" , "แบบฝึกหัดที่ 1")
            intent.putExtra("event" , "event_form_after_song_activity")
            startActivity(intent)
            finish()
        }

//        val mediaController = MediaController(this)
//        mediaController.setAnchorView(vdoView)
//        vdoView.setMediaController(mediaController)

        playVdoIntroduce(this , R.raw.introduce_acting)
    }

    private fun setActivityTab(tab:Int){
        menuActive = tab

        tvTab1Song.alpha = 0.7f
        tvTab2Song.alpha = 0.7f

        vLineTab1.visibility = View.INVISIBLE
        vLineTab2.visibility = View.INVISIBLE

        if( tab == 1 ){
            tvTab1Song.alpha = 1.0F
            vLineTab1.visibility = View.VISIBLE
        }else{
            tvTab2Song.alpha = 1.0F
            vLineTab2.visibility = View.VISIBLE
        }
    }

    private fun getFragmentByTag(tag:String):Fragment?{
        val fmManager = this@ParentSongActivity.supportFragmentManager
        return fmManager.findFragmentByTag(tag)!!
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if( data != null ){
            if( menuActive == 1 ){
                val fragment = getFragmentByTag("tab1")
                (fragment as Tab1SongFragment).onActivityResult(requestCode , resultCode , data)
            }else{
                val fragment = getFragmentByTag("tab2")
                (fragment as Tab2SongFragment).onActivityResult(requestCode , resultCode , data)
            }
        } else{
            Log.e("parentSongActivity" , "data == null")
        }
    }

    private fun initialFragment(pageName: String , seekTo:Int){
        val fmManager = this@ParentSongActivity.supportFragmentManager
        var newFragment: Fragment? = null
        var transaction: FragmentTransaction? = null
        var tag = ""

        when (pageName) {
            "tab1" -> {
                newFragment = Tab1SongFragment.newInstance(vocabData, data , seekTo)
                tag = "tab1"
            }
            "tab2" -> {
                newFragment = Tab2SongFragment.newInstance(seekTo)
                tag = "tab2"
            }
        }
        transaction = fmManager.beginTransaction()
        transaction.replace(R.id.flContainerTab, newFragment!!, tag)
        transaction.addToBackStack(tag)
        transaction.commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        finish()
    }
}