package com.rabumkai.thaidanceproject

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_costume.*


class CostumeActivity : BaseActivity() {
    var playAudio = MediaPlayer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var url = ""
        var titlePage = ""
        val bundle = intent.extras
        if( bundle != null ){
            url = bundle.getString("data").toString()
            titlePage = bundle.getString("title").toString()
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        }

        setContentView(R.layout.activity_costume)
        toolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        tvTitleNavigator.text = titlePage
        tvTitle.text = titlePage
        Glide.with(this)
            .load(R.drawable.banner_header_costume)
            .into(ivBannerHeader)

        ivBannerHeader.setBackgroundColor(resources.getColor(R.color.colorPrimary))

        llContent.visibility = View.GONE

        llRootNavigator.setOnClickListener {
            playAudio.stop()
            finish()
        }

        ivCostume1.setOnClickListener {
            playAudio.stop()
            llContent.visibility = View.GONE
        }

        ivCostume2.setOnClickListener {
            playAudio.stop()
            llContentBack.visibility = View.GONE
        }

        v1.setOnClickListener {
            playAudio(R.raw.a_1)
            showPopup(it.id)

        }
        v2.setOnClickListener {
            playAudio(R.raw.a_2)
            showPopup(it.id)
        }
        v3.setOnClickListener {
            playAudio(R.raw.a_3)
            showPopup(it.id)
        }
        v4.setOnClickListener {
            playAudio(R.raw.a_4)
            showPopup(it.id)
        }
        v5.setOnClickListener {
            playAudio(R.raw.a_5)
            showPopup(it.id)
        }
        v6.setOnClickListener {
            playAudio(R.raw.a_6)
            showPopup(it.id)
        }
        v7.setOnClickListener {
            playAudio(R.raw.a_7)
            showPopup(it.id)
        }
        v8.setOnClickListener {
            playAudio(R.raw.a_8)
            showPopup(it.id)
        }
        v9.setOnClickListener {
            playAudio(R.raw.a_9)
            showPopup(it.id)
        }
        v10.setOnClickListener {
            playAudio(R.raw.a_10)
            showPopup(it.id)
        }

        playVdoIntroduce(this , R.raw.introduce_costume)

        tvVdoIntroduce.setOnClickListener {
            playVdoIntroduce(this , R.raw.introduce_costume)
        }

        llNextChapter.setOnClickListener {
            val intent = Intent( this , MusicInstrumentActivity::class.java )
            val lstMusicalInstrumentModel = getMusicalInstrumentModel()
            val data = Gson().toJson(lstMusicalInstrumentModel)

            intent.putExtra("data" , data )
            intent.putExtra("title" , "เครื่องดนตรี" )
            intent.putExtra("event" , "after_costume_activity")
            startActivity(intent)

            finish()
        }
    }

    private fun showPopup(idTarget:Int ){

        if( idTarget == v9.id ){
            val set = ConstraintSet()
            set.clone(constraintCostume2)
            set.connect( llContentBack.id , ConstraintSet.BOTTOM , idTarget , ConstraintSet.TOP )
            set.connect( llContentBack.id , ConstraintSet.START , idTarget , ConstraintSet.START )
            set.connect( llContentBack.id , ConstraintSet.END , idTarget , ConstraintSet.END )
            set.applyTo(constraintCostume1)

            llContentBack.visibility = View.VISIBLE
            var nameTH = ""
            var nameEng = ""

            nameTH = "หางไก่"
            nameEng = "Chicken tail"

            tvNameTHBack.text = nameTH
            tvNameENGBack.text = nameEng
        }else{
            val set = ConstraintSet()
            set.clone(constraintCostume1)
            set.connect( llContent.id , ConstraintSet.BOTTOM , idTarget , ConstraintSet.TOP )
            set.connect( llContent.id , ConstraintSet.START , idTarget , ConstraintSet.START )
            set.connect( llContent.id , ConstraintSet.END , idTarget , ConstraintSet.END )
            set.applyTo(constraintCostume1)

            llContent.visibility = View.VISIBLE
            var nameTH = ""
            var nameEng = ""
            when (idTarget) {
                v1.id -> {
                    nameTH = "จี้นาง"
                    nameEng = "Jeenang-Pendant"
                }
                v2.id -> {
                    nameTH = "เกี้ยวตัวไก่"
                    nameEng = "Chicken Body Woo"
                }
                v3.id -> {
                    nameTH = "กรองคอ"
                    nameEng = "Krong kor- Throat cover"
                }
                v4.id -> {
                    nameTH = "ข้อมือ"
                    nameEng = "Wrist"
                }
                v5.id -> {
                    nameTH = "เสื้อ"
                    nameEng = "A-Shirt"
                }
                v6.id -> {
                    nameTH = "กางเกง"
                    nameEng = "Pants"
                }
                v7.id -> {
                    nameTH = "ปีกไก่"
                    nameEng = "Chicken wings"
                }
                v8.id -> {
                    nameTH = "ข้อเท้า"
                    nameEng = "Ankle"
                }
                v10.id -> {
                    nameTH = "สายเข็มขัด"
                    nameEng = "Belt strap"
                }
            }

            tvNameTH.text = nameTH
            tvNameENG.text = nameEng
        }
    }

    private fun playAudio(idResource:Int){
        playAudio.stop()
        playAudio = MediaPlayer.create(this , idResource)
        playAudio.start()
    }

    override fun onStop() {
        super.onStop()

        playAudio.stop()
    }
}