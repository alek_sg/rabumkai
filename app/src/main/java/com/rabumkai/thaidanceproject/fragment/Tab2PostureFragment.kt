package com.rabumkai.thaidanceproject.fragment

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.adapter.office.Extention.showPosture
import com.bumptech.glide.Glide
import com.rabumkai.thaidanceproject.BaseActivity
import com.rabumkai.thaidanceproject.FullScreenVideoActivity
import com.rabumkai.thaidanceproject.R
import kotlinx.android.synthetic.main.fragment_tab1_posture.*
import kotlinx.android.synthetic.main.fragment_tab2_posture.*
import kotlinx.android.synthetic.main.fragment_tab2_posture.tvVdoIntroduce
import kotlinx.android.synthetic.main.layout_action_bar.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class Tab2PostureFragment : Fragment() {

    var playAudio = MediaPlayer()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab2_posture, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivA.setOnClickListener {
            showDialog( tvA.text.toString() , "a" , tvDetailA.text.toString() , R.raw.a )
        }
        ivAA.setOnClickListener {
            showDialog( tvAA.text.toString() , "mmm" , tvDetailAA.text.toString() , R.raw.mmm )
        }
        ivB.setOnClickListener {
            showDialog( tvB.text.toString() , "b" , tvDetailB.text.toString() , R.raw.b)
        }
        ivC.setOnClickListener {
            showDialog( tvC.text.toString() , "c" , tvDetailC.text.toString() , R.raw.c)
        }
        ivCC.setOnClickListener {
            showDialog( tvCC.text.toString() , "cc" , tvDetailCC.text.toString() ,R.raw.cc)
        }
//        ivH.setOnClickListener {
//            showDialog( tvH.text.toString() , "h" , tvDetailH.text.toString() , R.raw.h)
//        }
//        ivEEEE.setOnClickListener {
//            showDialog( tvEEEE.text.toString() , "eeee" , tvDetailEEEE.text.toString() , R.raw.eeee)
//        }
        ivE.setOnClickListener {
            showDialog( tvE.text.toString() , "e" , tvDetailE.text.toString() , R.raw.e)
        }
        ivF.setOnClickListener {
            showDialog( tvF.text.toString() , "f" , tvDetailF.text.toString() , R.raw.f)
        }
//        ivG.setOnClickListener {
//            showDialog( tvG.text.toString() , "g" , tvDetailG.text.toString())
//        }
//        ivD.setOnClickListener {
//            showDialog( tvD.text.toString() , "d" , tvDetailD.text.toString() , R.raw.d)
//        }
//        ivI.setOnClickListener {
//            showDialog( tvI.text.toString() , "i" , tvDetailI.text.toString() , R.raw.i)
//        }

        ivI15.setOnClickListener {
            showDialog( tvI15.text.toString() , "mmm" , tvDetail15.text.toString() , R.raw.mmm)
        }


        tvVdoIntroduce.setOnClickListener {
            playVdoIntroduce()
        }
    }

    private fun playVdoIntroduce(){
        val intent = Intent( activity , FullScreenVideoActivity::class.java )
        intent.putExtra("seek" , 0)
        intent.putExtra("resource" ,R.raw.introduce_posture )
        startActivity(intent)
    }

    private fun showDialog(posture_name: String , img:String , detailPosture:String , idResource:Int ){

        val parent = activity as BaseActivity

        parent.showPosture().apply {
            val ivPosture = findViewById<ImageView>(R.id.ivMenu)
            val tvContent = findViewById<TextView>(R.id.tvContent)
            val cardView = findViewById<CardView>(R.id.cardView)
            val tvDetail = findViewById<TextView>(R.id.tvDetail)

            playAudio.stop()
            playAudio(idResource)

            this.setOnDismissListener {
                playAudio.stop()
                dismiss()
            }

            tvContent.text = posture_name
            tvDetail.text = detailPosture
            cardView.setOnClickListener {
                playAudio.stop()
                dismiss()
            }

            val resource = parent.getResourceId(context , img , "drawable" , parent.packageName)
            Glide.with(parent).load(resource).into(ivPosture)
            show()

            window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            window?.setBackgroundDrawableResource(android.R.color.transparent);
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if( playAudio != null ){
            playAudio.stop()
        }
    }

    private fun playAudio(idResource:Int){
        playAudio.stop()
        playAudio = MediaPlayer.create(requireContext() , idResource)
        playAudio.start()
    }
}