package com.rabumkai.thaidanceproject


import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.MediaController
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_acting.*
import kotlinx.android.synthetic.main.activity_acting.ivBannerHeader
import kotlinx.android.synthetic.main.activity_acting.ivFullScreen
import kotlinx.android.synthetic.main.activity_acting.llRootNavigator
import kotlinx.android.synthetic.main.activity_acting.toolbar
import kotlinx.android.synthetic.main.activity_acting.tvTitleNavigator


class ActingActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var titlePage = ""
        var data = ""
        val bundle = intent.extras
        if( bundle != null ){
            data = bundle.getString("data").toString()
            titlePage = bundle.getString("title")!!
        }

        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)

        setContentView(R.layout.activity_acting)

//        initial("https://www.youtube.com/watch?v=scWVoVR0WRc")
        var resource = 0
        if( bundle != null ){
            resource = bundle.getInt("resource")
        }

//        tvVdoIntroduce.setOnClickListener {
//            playVdoIntroduce(this , R.raw.introduce_acting)
//        }

//        playVdo(resource)

        toolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        tvTitleNavigator.text = titlePage
        Glide.with(this)
            .load(R.drawable.banner_header_acting)
            .into(ivBannerHeader)
        ivBannerHeader.setBackgroundColor(resources.getColor(R.color.colorPrimary))

        llRootNavigator.setOnClickListener {
            finish()
        }

        ivFullScreen.setOnClickListener {
            val intent = Intent( this@ActingActivity , FullScreenVideoActivity::class.java )
            intent.putExtra("seek" , vdoView.currentPosition)
            intent.putExtra("resource" ,resource )
            intent.putExtra("requestCode" ,1001 )

            startActivityForResult(intent , 1001)
        }

        ivPlayActing.visibility = View.VISIBLE
        ivPlayActing.setOnClickListener {
            playVdo(resource)
            vdoView.start()
            ivPlayActing.visibility = View.GONE
        }

        llNextChapter.setOnClickListener {
            // goto CostumeActivity

            val intent = Intent( this , CostumeActivity::class.java )
            val data = getCostume()
            intent.putExtra("data" , data )
            intent.putExtra("title" , "เครื่องแต่งกาย" )
            startActivity(intent)

            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if( data != null ){
            data!!.extras?.apply {
                vdoView.seekTo(getInt("seek"))
                vdoView.start()
            }
        }else{
            Log.e("ActingActivity" , "data == null")
        }

    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun playVdo(idResource:Int){
        try{
            val path = "android.resource://$packageName/$idResource"
            val u = Uri.parse(path)
            vdoView.setVideoURI(u)
            vdoView.setOnPreparedListener(MediaPlayer.OnPreparedListener { mp ->
                mp.isLooping = false
                mp.setScreenOnWhilePlaying(false)
            })
        }catch (e:Exception){
            Log.e("",""+e.toString())
        }
    }
    
     override fun onPause() {
        super.onPause()

        if( vdoView != null )
            vdoView.pause()
    }

    override fun onStop() {
        super.onStop()

        if( vdoView != null )
            vdoView.pause()
    }
}