package com.rabumkai.thaidanceproject.model


import com.google.gson.annotations.SerializedName

data class GameMatching(
    @SerializedName("content_posture_img")
    var contentPostureImg: ArrayList<ContentPostureImg>,
    @SerializedName("content_posture_name")
    var contentPostureName: ArrayList<ContentPostureName>
)