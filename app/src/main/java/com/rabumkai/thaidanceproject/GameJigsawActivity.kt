package com.rabumkai.thaidanceproject

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.iterator
import com.adapter.office.Extention.showDialogComplete
import com.adapter.office.Extention.showDialogWarning
import com.adapter.office.Extention.showMatch
import com.rabumkai.thaidanceproject.customView.MyView
import com.rabumkai.thaidanceproject.features.SoundPlayer
import kotlinx.android.synthetic.main.activity_game_jigsaw.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class GameJigsawActivity : BaseActivity() {
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.Main)
    var mSoundPlayer: SoundPlayer? = null
    var scoreCorrect = 0
    var scoreWrong = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        setContentView(R.layout.activity_game_jigsaw)

        mSoundPlayer = SoundPlayer(this , null)

        scope.launch {
            delay(500)
            bindView()
        }

        llRootNavigator.setOnClickListener {
            val dlgWarning = this.showDialogWarning("จะออกจากเกมตอนนี้เหรอ?", "ออกเลย")
            dlgWarning.window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            )
            dlgWarning.window?.setBackgroundDrawableResource(android.R.color.transparent);

            val tvPositive = dlgWarning.findViewById<TextView>(R.id.tvPositive)
            tvPositive.setOnClickListener {
                dlgWarning.dismiss()
                finish()
            }
            val tvNegative = dlgWarning.findViewById<TextView>(R.id.tvNegative)
            tvNegative.setOnClickListener {
                dlgWarning.dismiss()
            }

            dlgWarning.show()
        }

        llRestartGame.setOnClickListener {
            val dlgWarning = this.showDialogWarning("เริ่มเล่นเกมใหม่อีกครั้ง?", "เริ่มใหม่")
            dlgWarning.window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            )
            dlgWarning.window?.setBackgroundDrawableResource(android.R.color.transparent);

            val tvPositive = dlgWarning.findViewById<TextView>(R.id.tvPositive)
            tvPositive.setOnClickListener {
                dlgWarning.dismiss()

                //recreate activity
                val intent = intent
                finish()
                startActivity(intent)

            }
            val tvNegative = dlgWarning.findViewById<TextView>(R.id.tvNegative)
            tvNegative.setOnClickListener {
                dlgWarning.dismiss()
            }
            dlgWarning.show()
        }
    }

    private fun bindView(){
        val pieces: ArrayList<MyView> = ArrayList<MyView>()
        val touchListener = TouchListener(this@GameJigsawActivity)
        touchListener.setTouchCallBack(object : TouchListener.TouchCallBack {
            override fun gameOver() {
                if( isGameOver() )
                {
                    scope.launch {
                        val dlg = this@GameJigsawActivity.showDialogComplete().apply {
                            show()

                            window?.setLayout(
                                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                            window?.setBackgroundDrawableResource(android.R.color.transparent);
                        }

                        delay(1000)
                        val intent =
                            Intent(this@GameJigsawActivity
                                , SummaryScoreQuestionActivity::class.java)

                        intent.putExtra("score", scoreCorrect)
                        intent.putExtra("total", scoreCorrect)
                        intent.putExtra("count_not_matching", scoreWrong)
                        intent.putExtra("game_jigsaw", "เกมจิ๊กซอว์")
                        startActivity(intent)
                        finish()
                    }
                }
            }

            override fun correctPosition() {
                scoreCorrect++
                mSoundPlayer!!.play(SoundPlayer.Sound.Correct)
            }

            override fun wrongPosition() {
                scoreWrong++
                mSoundPlayer!!.play(SoundPlayer.Sound.Wrong)
            }
        } )

        for( i in 0..constraintContainer.childCount-1 ){

            if( constraintContainer.getChildAt(i) is ImageView ){
                val ivTmp = constraintContainer.getChildAt(i) as ImageView
                if( ivTmp.tag == null ){ // fillter imageview bg

                    val myView = MyView(this@GameJigsawActivity)
                    myView.canMove = true
                    myView.pieceWidth = ivTmp.width
                    myView.pieceHeight = ivTmp.height
                    myView.xCoord = ivTmp.left
                    myView.yCoord = ivTmp.top
                    pieces.add(myView)

                    ivTmp.visibility = View.VISIBLE
                    ivTmp.alpha = 0.2F
                }else{
                    ivTmp.visibility = View.INVISIBLE
                }
            }
        }

        for( i in 0..rlContainerPiece.childCount-1 ){

            if( rlContainerPiece.getChildAt(i) is ImageView ){
                val ivTmp = rlContainerPiece.getChildAt(i) as MyView
                val ran = rlContainerPiece.width - pieces[i].pieceWidth
                val maxX = (0..ran).random()
                val param = RelativeLayout.LayoutParams(pieces[i].pieceWidth, pieces[i].pieceHeight)
                param.topMargin = rlContainerPiece.height - ivTmp.height
                param.marginStart = maxX

                ivTmp.layoutParams = param
                ivTmp.canMove = true
                ivTmp.pieceHeight = pieces[i].pieceHeight
                ivTmp.pieceWidth = pieces[i].pieceWidth
                ivTmp.xCoord = pieces[i].xCoord
                ivTmp.yCoord = pieces[i].yCoord
                ivTmp.setOnTouchListener(touchListener)
            }
        }
    }

    override fun onBackPressed() {
//        super.onBackPressed()

        llRootNavigator.performClick()
    }

    private fun isGameOver(): Boolean {
//        return true

        for (piece in rlContainerPiece) {
            if ((piece as MyView).canMove) {
                return false
            }
        }
        return true
    }
}