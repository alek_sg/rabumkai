package com.rabumkai.thaidanceproject

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.rabumkai.thaidanceproject.model.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONObject
import java.io.IOException
import java.util.*

open class BaseActivity : AppCompatActivity() {

    private var jsonObject = JSONObject()
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val data = readJSONFromAsset("thai-dance-project-export.json")
        jsonObject = JSONObject(data)


    }

    private fun readJSONFromAsset(fileName: String?): String? {
        var json: String? = null
        json = try {
            val inputStream = this@BaseActivity.assets.open(fileName!!)
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    fun getMainMenu():ArrayList<MainMenuModel>{
        val jMainmenu = jsonObject.getJSONObject("dev").getJSONArray("mainmenu")
        val userListType = object : TypeToken<ArrayList<MainMenuModel?>?>() {}.type
        return Gson().fromJson(jMainmenu.toString(), userListType)
    }

    fun getMusicalInstrumentModel():ArrayList<MusicalInstrumentModel>{
        val jMusicalInstrumentModel = jsonObject.getJSONObject("dev").getJSONArray("musical_instrument")
        val userListType = object : TypeToken<ArrayList<MusicalInstrumentModel?>?>() {}.type
        return Gson().fromJson(jMusicalInstrumentModel.toString(), userListType)
    }

    fun getQuiz():ArrayList<QuizModel>{
        val jArrQuestion = jsonObject.getJSONObject("dev").getJSONArray("question")
        val userListType = object : TypeToken<ArrayList<QuizModel?>?>() {}.type
        return Gson().fromJson(jArrQuestion.toString(), userListType)
    }

    fun getPosture():ArrayList<PostureX>{

        val jArrPostureModel = jsonObject.getJSONObject("dev").getJSONArray("posture")
        val userListType = object : TypeToken<ArrayList<PostureX?>?>() {}.type
        return Gson().fromJson(jArrPostureModel.toString(), userListType)
    }

    fun getGameMatchingModel():GameMatching{

        val jObj = jsonObject.getJSONObject("dev").getJSONObject("game_matching")
        val model = Gson().fromJson(jObj.toString(), GameMatching::class.java)
        return model
    }

    fun getRandomContentMatchingModel():GameMatching{
        val lstData =  getGameMatchingModel()
        val lstContentPostureNameModel = ArrayList<ContentPostureName>()
        val lstContentPostureImageModel = ArrayList<ContentPostureImg>()

        while ( lstContentPostureNameModel.size < 7 ){

            val ran = (0..(lstData.contentPostureImg.size-1)).random()

            lstContentPostureNameModel.add(lstData.contentPostureName[ran])
            lstContentPostureImageModel.add(lstData.contentPostureImg[ran])

            lstData.contentPostureName.removeAt(ran)
            lstData.contentPostureImg.removeAt(ran)
        }
        return GameMatching(lstContentPostureImageModel, lstContentPostureNameModel)
    }

    fun getSong():ArrayList<Song>{

        val jArrPostureModel = jsonObject.getJSONObject("dev").getJSONArray("song")
        val userListType = object : TypeToken<ArrayList<Song?>?>() {}.type
        return Gson().fromJson(jArrPostureModel.toString(), userListType)
    }

    fun getDataVocab():ArrayList<VocabModel> {

        val jArrVocabModel = jsonObject.getJSONObject("dev").getJSONArray("vocap")
        val userListType = object : TypeToken<ArrayList<VocabModel?>?>() {}.type
        return Gson().fromJson(jArrVocabModel.toString(), userListType)
    }

    fun getCostume():String{
        return jsonObject.
                getJSONObject("dev").
                getJSONObject("costume").
                getString("value")

    }

    fun getHistory():String{
        return jsonObject.
                getJSONObject("dev").
                getJSONObject("history").
                getString("value")

    }

    fun getResourceId(
        context: Context,
        pVariableName: String?,
        pResourcename: String?,
        pPackageName: String?
    ): Int {
        return try {
            context.resources.getIdentifier(pVariableName, pResourcename, pPackageName)
        } catch (e: Exception) {
           Log.e("", "getResourseId-->ex = $e")
        }
    }

    fun isConnected():Boolean{
        var isConnected = false
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            isConnected = when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.run {
                activeNetworkInfo?.run {
                    isConnected = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }
                }
            }
        }

        return isConnected
    }

    fun playVdoIntroduce(activity: Activity, resourceIdVdo: Int){
        val intent = Intent(activity, FullScreenVideoActivity::class.java)
        intent.putExtra("seek", 0)
        intent.putExtra("resource", resourceIdVdo)
        startActivity(intent)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(AudioServiceContext.getContext(newBase))
    }
}