package com.rabumkai.thaidanceproject

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.rabumkai.thaidanceproject.model.RegisterModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.PropertyName
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if( !Hawk.isBuilt() ){
            Hawk.init(this).build()
        }

        if( Hawk.contains("profile") ){
            val registerModel = Hawk.get<RegisterModel>("profile")
            registerModel?.apply {
                edtUserName.setText(username)
                edtPassword.setText(password)

                checkAuthentication()
            }
        }

        btnLogin.setOnClickListener {
            if( isConnected() ){
                checkAuthentication()
            }else{
                Toast.makeText(this@LoginActivity , "กรุณาต่อ internet" , Toast.LENGTH_SHORT).show()
            }
        }

        tvAnonymous.setOnClickListener {

            Hawk.delete("profile")

            val intent = Intent( this@LoginActivity , TabActivity::class.java )

            val registerModel = RegisterModel()
            registerModel.name = ""
            registerModel.lastname = ""
            registerModel.classroom = ""
            registerModel.username = ""
            registerModel.password = ""
            registerModel.key = ""

            val data = Gson().toJson(registerModel)

            intent.putExtra("profile" , data)
            startActivityMainMenuActivity(intent)
        }

        tvRegister.setOnClickListener {
            this.startActivity(Intent(this@LoginActivity , RegisterActivity::class.java))
        }

        tvVersion.text = BuildConfig.VERSION_NAME.toString()

//        val lstModel = getRandomContentMatchingModel()
//        for( matchingGameModel: contentModel in lstModel ){
//            Log.e("LoginActivity" , "matchingGameModel-->"+matchingGameModel.toString())
//        }

        //test
//        val intent = Intent( this@LoginActivity , SummaryScoreQuestionActivity::class.java )
//        intent.putExtra("score" , 10)
//        intent.putExtra("total" , 10)
//        intent.putExtra("game_matching" , "เกมจับคู่")
//        intent.putExtra("count_not_matching" , 100)
//        startActivity(intent)

    }

    private fun startActivityMainMenuActivity( intent:Intent ){
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()

        Hawk.destroy()
    }

    private fun checkAuthentication(){

        val database = Firebase.database
        val myRef = database.getReference("dev/member_list")
        myRef.orderByChild("username").addListenerForSingleValueEvent(
            object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    Log.e("LoginActivity","error->detail = "+error.details+"\nerror->message = "+error.message)
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    var canLogin = false
                    var registerModel:RegisterModel ?= null

                    for( memberList in snapshot.children ){
                        registerModel = memberList?.getValue<RegisterModel>()

//                        dataSnapshot.getValue<Post>()
                        var username = memberList.child("username").value.toString()
                        var password = memberList.child("password").value.toString()

                        if( checkUserName(username) && checkPassword(password) ){

                            var username = memberList.child("username").value.toString()
                            var password = memberList.child("password").value.toString()
                            var classroom = memberList.child("classroom").value.toString()
                            var name = memberList.child("name").value.toString()
                            var lastname = memberList.child("lastname").value.toString()
                            var after_study = memberList.child("after_study").value.toString()
                            var before_study = memberList.child("before_study").value.toString()
                            var correct_or_wrong = memberList.child("correct_or_wrong").value.toString()
                            var fill_word = memberList.child("fill_word").value.toString()
                            var key = memberList.key.toString()
                            var is_done_exercise = memberList.child("is_done_exercise").value.toString()
                            var is_done_exercise_fill_word = memberList.child("is_done_exercise_fill_word").value.toString()

                            registerModel!!.username = username
                            registerModel!!.password = password
                            registerModel!!.classroom = classroom
                            registerModel!!.name = name
                            registerModel!!.lastname = lastname
                            registerModel!!.after_study = after_study
                            registerModel!!.before_study = before_study
                            registerModel!!.correct_or_wrong = correct_or_wrong
                            registerModel!!.fill_word = fill_word
                            registerModel!!.key = key
                            registerModel!!.is_done_exercise = is_done_exercise
                            registerModel!!.is_done_exercise_fill_word = is_done_exercise_fill_word

                            canLogin = true
                            break
                        }
                    }

                    val intent = Intent( this@LoginActivity , TabActivity::class.java )
                    var data = ""
                    if( canLogin ){

                        Log.e("","registerModel-->"+registerModel.toString())
                        data  = Gson().toJson( registerModel )
                        save("profile" , registerModel!!)
                        intent.putExtra("profile" , data)
                        startActivityMainMenuActivity(intent)
                    }else{
                        Toast.makeText(this@LoginActivity , resources.getString(R.string.alert_login) , Toast.LENGTH_SHORT).show()
                    }
                }
            })
    }

    private fun save(key:String , registerModel:RegisterModel){
        Hawk.put(key , registerModel)
    }

    private fun checkUserName(userName:String):Boolean{
        return userName == edtUserName.text.toString()
    }

    private fun checkPassword(password:String):Boolean{
        return password == edtPassword.text.toString()
    }
}