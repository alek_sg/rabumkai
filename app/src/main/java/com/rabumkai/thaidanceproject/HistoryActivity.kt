package com.rabumkai.thaidanceproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.android.synthetic.main.activity_history.tvTitle

class HistoryActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var titlePage = ""
        var data = ""
        val bundle = intent.extras
        if( bundle != null ){
            data = bundle.getString("data").toString()

            titlePage = bundle.getString("title")!!
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        }

        setContentView(R.layout.activity_history)

        toolbar.setBackgroundColor(resources.getColor(R.color.colorPrimary))
        tvTitle.text = "ประวัติความเป็นมาของระบำไก่"
        Glide.with(this)
            .load(R.drawable.banner_header_history)
            .into(ivBannerHeader)


        llRootNavigator.setOnClickListener {
            finish()
        }

        tvHistory.text = data

        playVdoIntroduce(this , R.raw.introduce_history)

        tvVdoIntroduce.setOnClickListener {
            playVdoIntroduce(this , R.raw.introduce_history)
        }

        llNextChapter.setOnClickListener {
            //ไป นาฎยศัพท์และภาษาท่า
            gotoPostureActivity()
            finish()
        }
    }

    private fun gotoPostureActivity(){
        val dataPosture = getPosture()
        val data = Gson().toJson(dataPosture)

        val intent = Intent( this , ParentPostureActivity::class.java )
        intent.putExtra("data" , data )
        intent.putExtra("title" , "นาฏยศัพท์และภาษาท่า" )
        startActivity(intent)
    }
}