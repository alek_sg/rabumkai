package com.rabumkai.thaidanceproject.fragment

import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import com.adapter.office.Extention.showVdo
import com.bumptech.glide.Glide
import com.rabumkai.thaidanceproject.BaseActivity
import com.rabumkai.thaidanceproject.FullScreenVideoActivity
import com.rabumkai.thaidanceproject.R
import com.rabumkai.thaidanceproject.customView.FullScreenVideoView
import kotlinx.android.synthetic.main.fragment_tab1_posture.*
/**
 * A placeholder fragment containing a simple view.
 */

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class Tab1PostureFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        return inflater.inflate(R.layout.fragment_tab1_posture, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Tab1PostureFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        Glide.with(this)
//            .load(R.drawable.jj)
//            .into(ivJJ)
//
//        Glide.with(this)
//            .load(R.drawable.ll)
//            .into(ivLL)
//
//        Glide.with(this)
//            .load(R.drawable.ii)
//            .into(ivII)
//
//        Glide.with(this)
//            .load(R.drawable.mm)
//            .into(ivMM)
//
//        Glide.with(this)
//            .load(R.drawable.kk)
//            .into(ivKK)
//
//        Glide.with(this)
//            .load(R.drawable.nn)
//            .into(ivNN)

        bindEvent()
        bindVDO(vdo1 , R.raw.body_language_1 , 10)
        bindVDO(vdo2 , R.raw.body_language_2 , 10)
        bindVDO(vdo3 , R.raw.body_language_3 , 10)
        bindVDO(vdo4 , R.raw.body_language_4 , 10)

//        bindVDO(fullScreenVideoView: FullScreenVideoView, idResource:Int, seekTo:Int )
    }

    private fun bindEvent(){
        flVdo1.setOnClickListener {
            showDialog(tvNamePosture1.text.toString() , R.raw.body_language_1)
        }

        flVdo2.setOnClickListener {
            showDialog(tvNamePosture2.text.toString() , R.raw.body_language_2)
        }

        flVdo3.setOnClickListener {
            showDialog(tvNamePosture3.text.toString() , R.raw.body_language_3)
        }

        flVdo4.setOnClickListener {
            showDialog(tvNamePosture4.text.toString() , R.raw.body_language_4)
        }



//        flContainerAnimal.setOnClickListener {
//            if( flContainerAnimal.tag.toString() == "show" ){
//                ivAnimalExpandLess.rotation = 0f
//                flContainerAnimal.tag = "hide"
//
//                llRootAnimal.visibility = View.VISIBLE
//            }else{
//                ivAnimalExpandLess.rotation = 180f
//                flContainerAnimal.tag = "show"
//
//                llRootAnimal.visibility = View.GONE
//            }
//        }
//
//        flContainerNature.setOnClickListener {
//            if( flContainerNature.tag.toString() == "show" ){
//                ivNatureExpandLess.rotation = 0f
//                flContainerNature.tag = "hide"
//
//                llRootNature.visibility = View.VISIBLE
//            }else{
//                ivNatureExpandLess.rotation = 180f
//                flContainerNature.tag = "show"
//
//                llRootNature.visibility = View.GONE
//            }
//        }

//        ivJJ.setOnClickListener {
//            showDialog(tvJJ.text.toString(), "jj")
//        }
//        ivLL.setOnClickListener {
//            showDialog(tvLL.text.toString(), "ll")
//        }
//        ivII.setOnClickListener {
//            showDialog(tvII.text.toString(), "ii")
//        }
//        ivMM.setOnClickListener {
//            showDialog(tvMM.text.toString(), "mm")
//        }
//        ivKK.setOnClickListener {
//            showDialog(tvKK.text.toString(), "kk")
//        }
//        ivNN.setOnClickListener {
//            showDialog(tvNN.text.toString(), "nn")
//        }

        tvVdoIntroduce.setOnClickListener {
            playVdoIntroduce()
        }
    }

    private fun bindVDO(fullScreenVideoView: VideoView, idResource:Int, seekTo:Int ){
//        val mediaController = MediaController(activity)
//        mediaController.setAnchorView(fullScreenVideoView)
//        fullScreenVideoView!!.setMediaController(mediaController)

        playVdo(fullScreenVideoView , idResource , seekTo)
    }

    private fun playVdo(fullScreenVideoView: VideoView , idResource:Int , seekTo:Int){
        try{
            val path = "android.resource://com.rabumkai.thaidanceproject/$idResource"
            fullScreenVideoView!!.setVideoURI(Uri.parse(path))
            fullScreenVideoView!!.setOnPreparedListener(MediaPlayer.OnPreparedListener { mp ->
                mp.isLooping = false
                mp.setScreenOnWhilePlaying(false)
                fullScreenVideoView!!.seekTo( seekTo )
            })
        }catch (e:Exception){
            Log.e("Tab1SongFragment", "playVdo-->$e")
        }
    }

    private fun playVdoIntroduce(){
        val intent = Intent(activity, FullScreenVideoActivity::class.java)
        intent.putExtra("seek", 0)
        intent.putExtra("resource", R.raw.introduce_posture)
        startActivity(intent)
    }

    private fun showDialog(posture_name: String, idResource:Int){

        val parent = activity as BaseActivity

        parent.showVdo().apply {
            val vdo = findViewById<VideoView>(R.id.vdo1)
            val tvContent = findViewById<TextView>(R.id.tvContent)
            val cardView = findViewById<CardView>(R.id.cardView)

            tvContent.text = posture_name

            cardView.setOnClickListener {
                vdo.stopPlayback()
                dismiss()
            }

            this.setOnDismissListener {
                vdo.stopPlayback()
                dismiss()
            }

            val mediaController = MediaController(activity)
            mediaController.setAnchorView(vdo)
            vdo.setMediaController(mediaController)
            vdo.start()

            playVdo(vdo , idResource , 0)
            show()

            window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
            )
            window?.setBackgroundDrawableResource(android.R.color.transparent);
        }
    }

//    override fun onDestroyView() {
//        super.onDestroyView()
//        if (binding.getRoot().getParent() != null)
//            (binding.getRoot().getParent() as ViewGroup).removeView(binding.getRoot())
//    }
}