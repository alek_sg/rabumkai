package com.rabumkai.thaidanceproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu_game.*

class MenuGameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_game)

        cvMenuJigsaw.setOnClickListener {

        }

        cvMenuMatching.setOnClickListener {

            val intent = Intent( this@MenuGameActivity , GameMatchingActivity::class.java )
            startActivity( intent )
        }
    }
}